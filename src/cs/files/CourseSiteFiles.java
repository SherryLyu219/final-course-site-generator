package cs.files;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import cs.CourseSiteGeneratorApp;
import cs.data.CourseSiteData;
import cs.data.LecturePropertype;
import cs.data.RecLabPropertype;
import cs.data.ScheduleItem;
import cs.data.TAType;
import cs.data.TeachingAssistantPrototype;
import cs.data.TimeSlot;
import cs.data.TimeSlot.DayOfWeek;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import javax.json.JsonObjectBuilder;
import org.apache.commons.io.FileUtils;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class CourseSiteFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    CourseSiteGeneratorApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_NAME = "name";
    static final String JSON_EMAIL = "email";
    static final String JSON_TYPE = "type";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_START_TIME = "time";
    static final String JSON_DAY_OF_WEEK = "day";
    static final String JSON_MONDAY = "monday";
    static final String JSON_TUESDAY = "tuesday";
    static final String JSON_WEDNESDAY = "wednesday";
    static final String JSON_THURSDAY = "thursday";
    static final String JSON_FRIDAY = "friday";
    static final String JSON_LECTURE = "lectures";
    static final String JSON_LLLLLECCCCTURE = "lecture";
    static final String JSON_SECTION ="section";
    static final String JSON_DAY = "days";
    static final String JSON_DDAAYY ="day";
    static final String JSON_TIME = "time";
    static final String JSON_ROOM = "room";
    static final String JSON_DAY_TIME = "day_time";
    static final String JSON_LOC = "location";
    static final String JSON_TA1 ="ta_1";
    static final String JSON_TA2 ="ta_2";
    static final String JSON_LAB ="labs";
    static final String JSON_REC ="recitations";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_NUMBER ="number";
    static final String JSON_SEMESTER="semester";
    static final String JSON_YEAR = "year";
    static final String JSON_TITLE = "title";
    static final String JSON_DATE = "date";
    static final String JSON_PAGES = "pages";
    static final String JSON_LOGO = "logos";
    static final String JSON_SRC="src";
    static final String JSON_FAVICON ="favicon";
    static final String JSON_NAVBAR = "navbar";
    static final String JSON_LEFT ="bottom_left";
    static final String JSON_RIGHT ="bottom_right";
    static final String JSON_INSTRUCTOR="instructor";
    static final String JSON_LINK ="link";
    static final String JSON_HOURS="hours";
    static final String JSON_HOLIDAYS="holidays";
    static final String JSON_MONTH="month";
    static final String JSON_TOPIC="topic";
    static final String JSON_LECTURES="lectures";
    static final String JSON_REFERENCE = "references";
    static final String JSON_RECITATION="recitations";
    static final String JSON_HWS="hws";
    static final String JSON_REEECCCTATION ="recitation";
    static final String JSON_DESCRIPTION = "description";
    static final String JSON_TOPICS = "topics";
    static final String JSON_PREREQUEST = "prerequisites";
    static final String JSON_OUTCOMES = "outcomes";
    static final String JSON_TEXTBOOKS = "textbooks";
    static final String JSON_GRADEDCOMPONENT = "gradedComponents";
    static final String JSON_GRADINGNOTE = "gradingNote";
    static final String JSON_ACADEMIC = "academicDishonesty";
    static final String JSON_SPECIAL = "specialAssistance";
    static final String JSON_START_DATE="startdate";
    static final String JSON_END_DATE="enddate";
    static final String JSON_START_MONTH="startingMondayMonth";
    static final String JSON_START_DAY="startingMondayDay";
    static final String JSON_END_MONTH="endingFridayMonth";
    static final String JSON_END_DAY="endingFridayDay";
    public CourseSiteFiles(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	CourseSiteData dataManager = (CourseSiteData)data;
        dataManager.reset();

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);
        
        // LOAD ALL THE GRAD TAs
        loadTAs(dataManager, json, JSON_GRAD_TAS);
        loadTAs(dataManager, json, JSON_UNDERGRAD_TAS);

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String startTime = jsonOfficeHours.getString(JSON_START_TIME);
            DayOfWeek dow = DayOfWeek.valueOf(jsonOfficeHours.getString(JSON_DAY_OF_WEEK));
            String name = jsonOfficeHours.getString(JSON_NAME);
            TeachingAssistantPrototype ta = dataManager.getTAWithName(name);
            TimeSlot timeSlot = dataManager.getTimeSlot(startTime);
            timeSlot.toggleTA(dow, ta);
        }
        //LOAD ALL THE LECTURE TABLE
        JsonArray jsonLectureArray = json.getJsonArray(JSON_LECTURE);
        for(int i = 0; i< jsonLectureArray.size(); i++){
            JsonObject jsonLecture = jsonLectureArray.getJsonObject(i);
            String lecSec = jsonLecture.getString(JSON_SECTION);
            String lecDay = jsonLecture.getString(JSON_DAY);
            String lecTime = jsonLecture.getString(JSON_TIME);
            String lecRoom = jsonLecture.getString(JSON_ROOM);  
            LecturePropertype lecture = new LecturePropertype(lecSec,lecDay,lecTime,lecRoom);
            dataManager.addToLecture(lecture);    
        }
        
        JsonArray jsonLabArray = json.getJsonArray(JSON_LAB);
         for(int i = 0; i< jsonLabArray.size(); i++){
            JsonObject jsonLab = jsonLabArray.getJsonObject(i);
            String labSec = jsonLab.getString(JSON_SECTION);
            String labDay_Time = jsonLab.getString(JSON_DAY_TIME);
            String labLocation = jsonLab.getString(JSON_LOC);
            String labTa1 = jsonLab.getString(JSON_TA1);  
            String labTa2 = jsonLab.getString(JSON_TA2);
            RecLabPropertype lab = new RecLabPropertype(labSec,labDay_Time,labLocation,labTa1,labTa2);
            dataManager.addToLab(lab);    
        }
        JsonArray jsonRecArray = json.getJsonArray(JSON_RECITATION);        
        for(int i = 0; i< jsonRecArray.size(); i++){
            JsonObject jsonRec = jsonRecArray.getJsonObject(i);
            String labSec = jsonRec.getString(JSON_SECTION);
            String labDay_Time = jsonRec.getString(JSON_DAY_TIME);
            String labLocation = jsonRec.getString(JSON_LOC);
            String labTa1 = jsonRec.getString(JSON_TA1);  
            String labTa2 = jsonRec.getString(JSON_TA2);
            RecLabPropertype rec = new RecLabPropertype(labSec,labDay_Time,labLocation,labTa1,labTa2);
            dataManager.addToRec(rec);    
        }
        //LOAD ALL THE SCHEDULE THINGS
        
        JsonArray jsonholidayArray = json.getJsonArray(JSON_HOLIDAYS);        
        for(int i = 0; i< jsonholidayArray.size(); i++){
            JsonObject jsonholiday = jsonholidayArray.getJsonObject(i);
//            String Month = jsonholiday.getString(JSON_MONTH);
//            String day = jsonholiday.getString(JSON_DDAAYY);
//            String Date = Month+"/"+day;
 String Date = jsonholiday.getString(JSON_DATE);
            String title = jsonholiday.getString(JSON_TITLE);
            String topic = jsonholiday.getString(JSON_TOPIC); 
            String link = jsonholiday.getString(JSON_LINK);
            ScheduleItem sch = new ScheduleItem(JSON_HOLIDAYS,Date,title,topic,link);
            dataManager.addToSch(sch);    
        }
        
        JsonArray jsonLecArray = json.getJsonArray(JSON_LLLLLECCCCTURE);        
        for(int i = 0; i< jsonLecArray.size(); i++){
            JsonObject jsonholiday = jsonLecArray.getJsonObject(i);
//            String Month = jsonholiday.getString(JSON_MONTH);
//            String day = jsonholiday.getString(JSON_DDAAYY);
//            String Date = Month+"/"+day;
 String Date = jsonholiday.getString(JSON_DATE);
            String title = jsonholiday.getString(JSON_TITLE);
            String topic = jsonholiday.getString(JSON_TOPIC); 
            String link = jsonholiday.getString(JSON_LINK);
            ScheduleItem sch = new ScheduleItem(JSON_LECTURES,Date,title,topic,link);
            dataManager.addToSch(sch);    
        }
        
        JsonArray jsonRefArray = json.getJsonArray(JSON_REFERENCE);        
        for(int i = 0; i< jsonRefArray.size(); i++){
            JsonObject jsonholiday = jsonRefArray.getJsonObject(i);
//            String Month = jsonholiday.getString(JSON_MONTH);
//            String day = jsonholiday.getString(JSON_DDAAYY);
//            String Date = Month+"/"+day;
 String Date = jsonholiday.getString(JSON_DATE);
            String title = jsonholiday.getString(JSON_TITLE);
            String topic = jsonholiday.getString(JSON_TOPIC);  
            String link = jsonholiday.getString(JSON_LINK);
            ScheduleItem sch = new ScheduleItem(JSON_REFERENCE,Date,title,topic,link);
            dataManager.addToSch(sch);    
        }
        JsonArray jsonRecitaArray = json.getJsonArray(JSON_REEECCCTATION);        
        for(int i = 0; i< jsonRecitaArray.size(); i++){
            JsonObject jsonholiday = jsonRecitaArray.getJsonObject(i);
//            String Month = jsonholiday.getString(JSON_MONTH);
//            String day = jsonholiday.getString(JSON_DDAAYY);
//            String Date = Month+"/"+day;
 String Date = jsonholiday.getString(JSON_DATE);
            String title = jsonholiday.getString(JSON_TITLE);
            String topic = jsonholiday.getString(JSON_TOPIC);  
            String link = jsonholiday.getString(JSON_LINK);
            ScheduleItem sch = new ScheduleItem(JSON_REEECCCTATION,Date,title,topic,link);
            dataManager.addToSch(sch);    
        }
        JsonArray jsonhwArray = json.getJsonArray(JSON_HWS);        
        for(int i = 0; i< jsonhwArray.size(); i++){
            JsonObject jsonholiday = jsonhwArray.getJsonObject(i);
//            String Month = jsonholiday.getString(JSON_MONTH);
//            String day = jsonholiday.getString(JSON_DDAAYY);
//            String Date = Month+"/"+day;
            String Date = jsonholiday.getString(JSON_DATE);
            String title = jsonholiday.getString(JSON_TITLE);
            String topic = jsonholiday.getString(JSON_TOPIC); 
            String link = jsonholiday.getString(JSON_LINK);
            ScheduleItem sch = new ScheduleItem(JSON_HWS,Date,title,topic,link);
            dataManager.addToSch(sch);    
        }
        //LOAD ALL THE SYALLUBUS THINGS
        String description = json.getString(JSON_DESCRIPTION);
        dataManager.addToSyllubus(description, JSON_DESCRIPTION);
        String topics = json.getString(JSON_TOPICS);
        /*
        JsonArray jsonTopicsArray = json.getJsonArray(JSON_TOPICS);
        String topics = "";
        for(int i = 0; i < jsonTopicsArray.size(); i++){
            String one = jsonTopicsArray.getString(i);
            topics = topics+one +"\n";
           
        }
        */
         dataManager.addToSyllubus(topics, JSON_TOPICS);
        
        String prerequisites = json.getString(JSON_PREREQUEST);
        dataManager.addToSyllubus(prerequisites, JSON_PREREQUEST);
        
         String outcomes = json.getString(JSON_OUTCOMES);
         /*
        JsonArray jsonOutcomesArray = json.getJsonArray(JSON_OUTCOMES);
        String outcomes = "";
        for(int i = 0; i < jsonOutcomesArray.size(); i++){
            String one = jsonOutcomesArray.getString(i);
           outcomes = outcomes +one +"\n";
        }
         */
         dataManager.addToSyllubus(outcomes, JSON_OUTCOMES);
       /*
        JsonArray jsonTextbookArray = json.getJsonArray(JSON_TEXTBOOKS);
        String textbooks = "";
        for(int i = 0; i < jsonTextbookArray.size(); i++){
            JsonArray innerTextArray = jsonTextbookArray.getString(i);
           textbooks = textbooks +one +"\n";
        }
        dataManager.addToSyllubus(textbooks, JSON_TEXTBOOKS);
        */
        String textbooks = json.getString(JSON_TEXTBOOKS);
        dataManager.addToSyllubus(textbooks, JSON_TEXTBOOKS);
        String gradedComponents = json.getString(JSON_GRADEDCOMPONENT);
        dataManager.addToSyllubus(gradedComponents, JSON_GRADEDCOMPONENT);
        String gradingNote = json.getString(JSON_GRADINGNOTE);
        dataManager.addToSyllubus(gradingNote, JSON_GRADINGNOTE);
        String academicDishonesty = json.getString(JSON_ACADEMIC);
        dataManager.addToSyllubus(academicDishonesty, JSON_ACADEMIC);
        String specialAssistance = json.getString(JSON_SPECIAL);
        dataManager.addToSyllubus(specialAssistance, JSON_SPECIAL);
        //LOAD ALL THE SITE PAGE THING
        String subject = json.getString(JSON_SUBJECT);
        dataManager.addToSite(subject,JSON_SUBJECT);
        String number = json.getString(JSON_NUMBER);
        dataManager.addToSite(number,JSON_NUMBER);
        String semester = json.getString(JSON_SEMESTER);
        dataManager.addToSite(semester, JSON_SEMESTER);
        String year = json.getString(JSON_YEAR);
        dataManager.addToSite(year, JSON_YEAR);
        String title = json.getString(JSON_TITLE);
        dataManager.addToSite(title, JSON_TITLE);
        
        JsonArray jsonPageArray = json.getJsonArray(JSON_PAGES);
        for(int i = 0; i < jsonPageArray.size(); i++){
             JsonObject jsonPage = jsonPageArray.getJsonObject(i);
             String name = jsonPage.getString(JSON_NAME);
             dataManager.addPageClick(name);
        }
        
        JsonObject jsonlogos = json.getJsonObject(JSON_LOGO);
        JsonObject jsonFav = jsonlogos.getJsonObject(JSON_FAVICON);
        String FavImagePath = jsonFav.getString(JSON_SRC);
        dataManager.addImage("favicon",FavImagePath);
        
        JsonObject jsonNavBar = jsonlogos.getJsonObject(JSON_NAVBAR);
        String NavImagePath =  jsonNavBar.getString(JSON_SRC);
        dataManager.addImage("navbar",NavImagePath);
        
        JsonObject jsonLeft = jsonlogos.getJsonObject(JSON_LEFT);
        String LeftImagePath = jsonLeft.getString(JSON_SRC);
        dataManager.addImage("Left", LeftImagePath);
        
        JsonObject jsonRight = jsonlogos.getJsonObject(JSON_RIGHT);
        String RightImagePath = jsonRight.getString(JSON_SRC);
        dataManager.addImage("Right",RightImagePath);
        
        JsonObject jsonInstructor = json.getJsonObject(JSON_INSTRUCTOR);
        String InstructorName = jsonInstructor.getString(JSON_NAME);
        dataManager.addInstructor("name",InstructorName);
        String InstructorLink = jsonInstructor.getString(JSON_LINK);
        dataManager.addInstructor("homepage", InstructorLink);
        String InstructorEmail = jsonInstructor.getString(JSON_EMAIL);
        dataManager.addInstructor("email", InstructorEmail);
        String InstructorRoom = jsonInstructor.getString(JSON_ROOM);
        dataManager.addInstructor("room", InstructorRoom);
        String InstructorOh = jsonInstructor.getString(JSON_HOURS);
        dataManager.addInstructor("oh", InstructorOh);
        String DateStart = json.getString(JSON_START_DATE);
        dataManager.addToDatePicker(DateStart,"start");
        String DateEnd = json.getString(JSON_END_DATE);
        dataManager.addToDatePicker(DateEnd,"end");
        
        
        
        
    }
    
    private void loadTAs(CourseSiteData data, JsonObject json, String tas) {
        JsonArray jsonTAArray = json.getJsonArray(tas);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            TAType type = TAType.valueOf(jsonTA.getString(JSON_TYPE));
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name, email, type);
            data.addTA(ta);
        }     
    }
   
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	CourseSiteData dataManager = (CourseSiteData)data;

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder gradTAsArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder undergradTAsArrayBuilder = Json.createArrayBuilder();
	Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        while (tasIterator.hasNext()) {
            TeachingAssistantPrototype ta = tasIterator.next();
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_TYPE, ta.getType().toString()).build();
            if (ta.getType().equals(TAType.Graduate.toString()))
                gradTAsArrayBuilder.add(taJson);
            else
                undergradTAsArrayBuilder.add(taJson);
	}
        JsonArray gradTAsArray = gradTAsArrayBuilder.build();
	JsonArray undergradTAsArray = undergradTAsArrayBuilder.build();

	// NOW BUILD THE OFFICE HOURS JSON OBJCTS TO SAVE
	JsonArrayBuilder officeHoursArrayBuilder = Json.createArrayBuilder();
        Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            for (int i = 0; i < DayOfWeek.values().length; i++) {
                DayOfWeek dow = DayOfWeek.values()[i];
                tasIterator = timeSlot.getTAsIterator(dow);
                while (tasIterator.hasNext()) {
                    TeachingAssistantPrototype ta = tasIterator.next();
                    JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_START_TIME, timeSlot.getStartTime().replace(":", "_"))
                        .add(JSON_DAY_OF_WEEK, dow.toString())
                        .add(JSON_NAME, ta.getName()).build();
                    officeHoursArrayBuilder.add(tsJson);
                }
            }
	}
	JsonArray officeHoursArray = officeHoursArrayBuilder.build();
        //BUILD THE LOGOS JSON OBJECT TO SAVE
        JsonObject faviconJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Fav")).build();
        JsonObject navbarconJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Nav")).build();
        JsonObject leftJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Left")).build();
        JsonObject rightJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Right")).build();
         JsonObject logos = Json.createObjectBuilder()
                 .add(JSON_FAVICON, faviconJson)
                 .add(JSON_NAVBAR, navbarconJson)
                 .add(JSON_LEFT, leftJson)
                 .add(JSON_RIGHT, rightJson)
                 .build();
        //BUILD INSTRUCTOR JSON OBJECT TO SAVE
        JsonObject instructor = Json.createObjectBuilder()
               .add(JSON_NAME,""+ dataManager.getInstruname())
                .add(JSON_LINK,""+ dataManager.getInstrulink())
                .add(JSON_EMAIL,""+ dataManager.getInstruemail())
                .add(JSON_ROOM,""+ dataManager.getInstruroom())
                .add(JSON_HOURS,""+ dataManager.getInstruhours())
                .build();
        //BUILD PAGE JSON OBJECT TO SAVE
        JsonArrayBuilder pagesArrayBuilder = Json.createArrayBuilder();
        if(dataManager.getHome()){
                JsonObject homeJson = Json.createObjectBuilder()
                        .add(JSON_NAME, "Home")
                        .add(JSON_LINK, "index.html")
                        .build();
                    pagesArrayBuilder.add(homeJson);
            
        }
        if(dataManager.getSyllabus()){
             JsonObject syll = Json.createObjectBuilder()
                        .add(JSON_NAME, "Syllabus")
                        .add(JSON_LINK, "syllabus.html")
                        .build();
                    pagesArrayBuilder.add(syll);
            
        }
        if(dataManager.getSchedule()){
            JsonObject syll = Json.createObjectBuilder()
                        .add(JSON_NAME, "Schedule")
                        .add(JSON_LINK, "schedule.html")
                        .build();
                    pagesArrayBuilder.add(syll);
        }
        if(dataManager.getHWs()){
            JsonObject syll = Json.createObjectBuilder()
                        .add(JSON_NAME, "HWs")
                        .add(JSON_LINK, "hws.html")
                        .build();
                    pagesArrayBuilder.add(syll);
        }
        JsonArray pageArray = pagesArrayBuilder.build();
        
        //BUILD THE MeetingTime OBJECT
        JsonArrayBuilder LectureArrayBuilder = Json.createArrayBuilder();
        Iterator<LecturePropertype> LectureIterator = dataManager.LectureIterator();
        while (LectureIterator.hasNext()) {
                LecturePropertype le = LectureIterator.next();
                JsonObject leJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, le.getSection())
                        .add(JSON_DAY, le.getDay())
                        .add(JSON_TIME, le.getTime())
                        .add(JSON_ROOM,le.getRoom())
                        .build();
                    LectureArrayBuilder.add(leJson);

	}
	JsonArray LectureArray = LectureArrayBuilder.build();
        
        
        JsonArrayBuilder RecArrayBuilder = Json.createArrayBuilder();
        Iterator<RecLabPropertype> RecIterator = dataManager.RecIterator();
        while (RecIterator.hasNext()) {
                RecLabPropertype rec = RecIterator.next();
                JsonObject recJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, rec.getSection())
                        .add(JSON_DAY_TIME, rec.getDay_time())
                        .add(JSON_LOC,rec.getRoom())
                        .add(JSON_TA1,rec.getTa1())
                        .add(JSON_TA2, rec.getTa2())
                        .build();
                    RecArrayBuilder.add(recJson);

	}
	JsonArray RecArray = RecArrayBuilder.build();
        
        JsonArrayBuilder LabArrayBuilder = Json.createArrayBuilder();
        Iterator<RecLabPropertype> LabIterator = dataManager.LabIterator();
        while (LabIterator.hasNext()) {
                RecLabPropertype rec = LabIterator.next();
                JsonObject recJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, rec.getSection())
                        .add(JSON_DAY_TIME, rec.getDay_time())
                        .add(JSON_LOC,rec.getRoom())
                        .add(JSON_TA1,rec.getTa1())
                        .add(JSON_TA2, rec.getTa2())
                        .build();
                    LabArrayBuilder.add(recJson);

	}
	JsonArray LabArray = LabArrayBuilder.build();
        //SAVE ALL THE SCH TABLE
        
        JsonArrayBuilder HolidayArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder LecturesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder ReferArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder RecitaArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder HwsArrayBuilder = Json.createArrayBuilder();
        Iterator<ScheduleItem> schIterator = dataManager.SchIterator();
        while (schIterator.hasNext()) {
                ScheduleItem sch = schIterator.next();
               
                if(sch.getType().equals(JSON_HOLIDAYS)){
                JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_DATE, sch.getDate())
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    HolidayArrayBuilder.add(schJson);
                }
                else if(sch.getType().equals("lectures")){
                     JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_DATE, sch.getDate())
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    LecturesArrayBuilder.add(schJson);
                }else if(sch.getType().equals("references")){
                    JsonObject schJson = Json.createObjectBuilder()
                       .add(JSON_DATE, sch.getDate())
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    ReferArrayBuilder.add(schJson);
                }else if(sch.getType().equals(JSON_REEECCCTATION)){
                    JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_DATE, sch.getDate())
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    RecitaArrayBuilder.add(schJson);
                }else{
                    JsonObject schJson = Json.createObjectBuilder()
                      .add(JSON_DATE, sch.getDate())
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    HwsArrayBuilder.add(schJson);
                }

	}
	JsonArray HolidayArray = HolidayArrayBuilder.build();
        JsonArray LecturesArray = LecturesArrayBuilder.build();
        JsonArray ReferArray = ReferArrayBuilder.build();
        JsonArray RecitaArray = RecitaArrayBuilder.build();
        JsonArray HwsArray = HwsArrayBuilder.build();
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, officeHoursArray)
                .add(JSON_SUBJECT,""+dataManager.getSubject())
                .add(JSON_NUMBER,""+dataManager.getNumber())
                .add(JSON_SEMESTER,""+dataManager.getSemester())
                .add(JSON_YEAR,""+dataManager.getYear())
                .add(JSON_TITLE,""+dataManager.getTitle())
                .add(JSON_LOGO,logos)
                .add(JSON_INSTRUCTOR, instructor)
                .add(JSON_PAGES, pageArray)
                .add(JSON_DESCRIPTION, dataManager.getDescription())
                .add(JSON_TOPICS, dataManager.getTopic())
                .add(JSON_PREREQUEST, dataManager.getPrerequisites())
                .add(JSON_OUTCOMES, dataManager.getOutcomes())
                .add(JSON_TEXTBOOKS, dataManager.getTextbooks())
                .add(JSON_GRADEDCOMPONENT, dataManager.getGradedComponents())
                .add(JSON_GRADINGNOTE,dataManager.getGradingNote())
                .add(JSON_ACADEMIC,dataManager.getAcademicDishonesty())
                .add(JSON_SPECIAL, dataManager.getSpecialAssistance())
                .add(JSON_LECTURE, LectureArray)
                .add(JSON_REC, RecArray)
                .add(JSON_LAB, LabArray)
                .add(JSON_HOLIDAYS,HolidayArray)
                .add(JSON_LLLLLECCCCTURE, LecturesArray)
                .add(JSON_REFERENCE, ReferArray)
                .add(JSON_REEECCCTATION,RecitaArray)
		.add(JSON_HWS, HwsArray)
                .add(JSON_START_DATE, dataManager.getDatePicker())
                .add(JSON_END_DATE,dataManager.getEndDate())
                .build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
       
       CourseSiteData dataManager = (CourseSiteData)data;
       String dir = dataManager.getExpor();
       String di[] = dir.split("/");
       File theDir = new File("./export/"+di[2]);
       theDir.mkdir();
       File theFinal = new File("./export/"+di[2]+"/"+di[3]);
       theFinal.mkdir();
      // System.out.print(dir);
       
        JsonObject faviconJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Fav")).build();
        JsonObject navbarconJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Nav")).build();
        JsonObject leftJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Left")).build();
        JsonObject rightJson = Json.createObjectBuilder()
                .add(JSON_SRC,dataManager.getImagePath("Right")).build();
         JsonObject logos = Json.createObjectBuilder()
                 .add(JSON_FAVICON, faviconJson)
                 .add(JSON_NAVBAR, navbarconJson)
                 .add(JSON_LEFT, leftJson)
                 .add(JSON_RIGHT, rightJson)
                 .build();
        String Instruhours = dataManager.getInstruhours();
        JsonArray arrInstru;
        if(Instruhours.equals("")){
            JsonArrayBuilder InstruArrayBuilder = Json.createArrayBuilder();
            arrInstru = InstruArrayBuilder.build();
        }else{
        JsonReader Instruhourr = Json.createReader(new StringReader(Instruhours));
        arrInstru= Instruhourr.readArray();
        }
        JsonObject instructor = Json.createObjectBuilder()
               .add(JSON_NAME,""+ dataManager.getInstruname())
                .add(JSON_LINK,""+ dataManager.getInstrulink())
                .add(JSON_EMAIL,""+ dataManager.getInstruemail())
                .add(JSON_ROOM,""+ dataManager.getInstruroom())
                .add(JSON_HOURS, arrInstru)
                .build();
        //BUILD PAGE JSON OBJECT TO SAVE
        JsonArrayBuilder pagesArrayBuilder = Json.createArrayBuilder();
        if(dataManager.getHome()){
                JsonObject homeJson = Json.createObjectBuilder()
                        .add(JSON_NAME, "Home")
                        .add(JSON_LINK, "index.html")
                        .build();
                    pagesArrayBuilder.add(homeJson);
            
        }
        if(dataManager.getSyllabus()){
             JsonObject syll = Json.createObjectBuilder()
                        .add(JSON_NAME, "Syllabus")
                        .add(JSON_LINK, "syllabus.html")
                        .build();
                    pagesArrayBuilder.add(syll);
            
        }
        if(dataManager.getSchedule()){
            JsonObject syll = Json.createObjectBuilder()
                        .add(JSON_NAME, "Schedule")
                        .add(JSON_LINK, "schedule.html")
                        .build();
                    pagesArrayBuilder.add(syll);
        }
        if(dataManager.getHWs()){
            JsonObject syll = Json.createObjectBuilder()
                        .add(JSON_NAME, "HWs")
                        .add(JSON_LINK, "hws.html")
                        .build();
                    pagesArrayBuilder.add(syll);
        }
        
        JsonArray pageArray = pagesArrayBuilder.build();
        JsonObject pageJson = Json.createObjectBuilder()
                .add(JSON_SUBJECT,""+dataManager.getSubject())
                .add(JSON_NUMBER,""+dataManager.getNumber())
                .add(JSON_SEMESTER,""+dataManager.getSemester())
                .add(JSON_YEAR,""+dataManager.getYear())
                .add(JSON_TITLE,""+dataManager.getTitle())
                .add(JSON_LOGO,logos)
                .add(JSON_INSTRUCTOR, instructor)
                .add(JSON_PAGES, pageArray)
                .build();
        
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(pageJson);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream("./export/sample/PageData.json");
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(pageJson);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter("./export/sample/PageData.json");
	pw.write(prettyPrinted);
	pw.close();
        //________________________________________________________________________________________
        JsonArrayBuilder gradTAsArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder undergradTAsArrayBuilder = Json.createArrayBuilder();
	Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        while (tasIterator.hasNext()) {
            TeachingAssistantPrototype ta = tasIterator.next();
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail()).build();
                   // .add(JSON_TYPE, ta.getType().toString()).build();
            if (ta.getType().equals(TAType.Graduate.toString()))
                gradTAsArrayBuilder.add(taJson);
            else
                undergradTAsArrayBuilder.add(taJson);
	}
        JsonArray gradTAsArray = gradTAsArrayBuilder.build();
	JsonArray undergradTAsArray = undergradTAsArrayBuilder.build();
        
        JsonArrayBuilder officeHoursArrayBuilder = Json.createArrayBuilder();
        Iterator<TimeSlot> timeSlotsIterator = dataManager.officeHoursIterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            for (int i = 0; i < DayOfWeek.values().length; i++) {
                DayOfWeek dow = DayOfWeek.values()[i];
                tasIterator = timeSlot.getTAsIterator(dow);
                while (tasIterator.hasNext()) {
                    TeachingAssistantPrototype ta = tasIterator.next();
                    JsonObject tsJson = Json.createObjectBuilder()
                        .add(JSON_START_TIME, timeSlot.getStartTime().replace(":", "_"))
                        .add(JSON_DAY_OF_WEEK, dow.toString())
                        .add(JSON_NAME, ta.getName()).build();
                    officeHoursArrayBuilder.add(tsJson);
                }
            }
	}
        
	JsonArray officeHoursArray = officeHoursArrayBuilder.build();
         JsonObject syllabusJson = Json.createObjectBuilder()
                .add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_INSTRUCTOR, instructor)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, officeHoursArray)
                .build();
        
        Map<String, Object> ohproperties = new HashMap<>(1);
	ohproperties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerohFactory = Json.createWriterFactory(ohproperties);
	StringWriter ohsw = new StringWriter();
	JsonWriter jsonohWriter = writerohFactory.createWriter(ohsw);
	jsonohWriter.writeObject(syllabusJson);
	jsonohWriter.close();

	// INIT THE WRITER
	OutputStream ohos = new FileOutputStream("./export/sample/OfficeHoursData.json");
	JsonWriter jsonohFileWriter = Json.createWriter(ohos);
	jsonohFileWriter.writeObject(syllabusJson);
	String prettyohPrinted = ohsw.toString();
	PrintWriter ohpw = new PrintWriter("./export/sample/OfficeHoursData.json");
	ohpw.write(prettyohPrinted);
	ohpw.close();
        //_____________________________________________________________________________________
         JsonArrayBuilder LectureArrayBuilder = Json.createArrayBuilder();
        Iterator<LecturePropertype> LectureIterator = dataManager.LectureIterator();
        while (LectureIterator.hasNext()) {
                LecturePropertype le = LectureIterator.next();
                JsonObject leJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, le.getSection())
                        .add(JSON_DAY, le.getDay())
                        .add(JSON_TIME, le.getTime())
                        .add(JSON_ROOM,le.getRoom())
                        .build();
                    LectureArrayBuilder.add(leJson);

	}
	JsonArray LectureArray = LectureArrayBuilder.build();
        
        
        JsonArrayBuilder RecArrayBuilder = Json.createArrayBuilder();
        Iterator<RecLabPropertype> RecIterator = dataManager.RecIterator();
        while (RecIterator.hasNext()) {
                RecLabPropertype rec = RecIterator.next();
                JsonObject recJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, rec.getSection())
                        .add(JSON_DAY_TIME, rec.getDay_time())
                        .add(JSON_LOC,rec.getRoom())
                        .add(JSON_TA1,rec.getTa1())
                        .add(JSON_TA2, rec.getTa2())
                        .build();
                    RecArrayBuilder.add(recJson);

	}
	JsonArray RecArray = RecArrayBuilder.build();
        
        JsonArrayBuilder LabArrayBuilder = Json.createArrayBuilder();
        Iterator<RecLabPropertype> LabIterator = dataManager.LabIterator();
        while (LabIterator.hasNext()) {
                RecLabPropertype rec = LabIterator.next();
                JsonObject recJson = Json.createObjectBuilder()
                        .add(JSON_SECTION, rec.getSection())
                        .add(JSON_DAY_TIME, rec.getDay_time())
                        .add(JSON_LOC,rec.getRoom())
                        .add(JSON_TA1,rec.getTa1())
                        .add(JSON_TA2, rec.getTa2())
                        .build();
                    LabArrayBuilder.add(recJson);

	}
	JsonArray LabArray = LabArrayBuilder.build();
        JsonObject sectionJson = Json.createObjectBuilder()
                .add(JSON_LECTURE, LectureArray)
                .add(JSON_LAB, LabArray)
                .add(JSON_REC, RecArray)
                .build();
        
        Map<String, Object> seproperties = new HashMap<>(1);
	seproperties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerseFactory = Json.createWriterFactory(seproperties);
	StringWriter sesw = new StringWriter();
	JsonWriter jsonseWriter = writerseFactory.createWriter(sesw);
	jsonseWriter.writeObject(sectionJson);
	jsonseWriter.close();

	// INIT THE WRITER
	OutputStream seos = new FileOutputStream("./export/sample/SectionsData.json");
	JsonWriter jsonseFileWriter = Json.createWriter(seos);
	jsonseFileWriter.writeObject(sectionJson);
	String prettysePrinted = sesw.toString();
	PrintWriter sepw = new PrintWriter("./export/sample/SectionsData.json");
	sepw.write(prettysePrinted);
	sepw.close();
        //_______________________________________________________________________________
        String topic = dataManager.getTopic();
        JsonArray arrtopic;
        if(topic.equals("")){
            JsonArrayBuilder topicArrayBuilder = Json.createArrayBuilder();
            arrtopic = topicArrayBuilder.build();
        }else{
        JsonReader topicr = Json.createReader(new StringReader(topic));
        arrtopic = topicr.readArray();
        }
        
        String outcomes = dataManager.getOutcomes();
        JsonArray arroutcomes;
        if(outcomes.equals("")){
            JsonArrayBuilder outcomesArrayBuilder = Json.createArrayBuilder();
            arroutcomes = outcomesArrayBuilder.build();
        }else{
        JsonReader outcomesr = Json.createReader(new StringReader(outcomes));
        arroutcomes = outcomesr.readArray();
        }
        
        String textBook = dataManager.getTextbooks();
        JsonArray arrtextBook;
        if(textBook.equals("")){
            JsonArrayBuilder tBArrayBuilder = Json.createArrayBuilder();
            arrtextBook = tBArrayBuilder.build();
        }else{
        JsonReader textBookr = Json.createReader(new StringReader(textBook));
         arrtextBook = textBookr.readArray();
        }
        
        String gradingComponet = dataManager.getGradedComponents();
        JsonArray arrgradingCompnent;
        if(gradingComponet.equals("")){
            JsonArrayBuilder ArrayBuilder = Json.createArrayBuilder();
            arrgradingCompnent = ArrayBuilder.build();
        }else{
        JsonReader gradingComponetr = Json.createReader(new StringReader(gradingComponet));
        arrgradingCompnent = gradingComponetr.readArray();
        }
        
        JsonObject syllJson = Json.createObjectBuilder()
                .add(JSON_DESCRIPTION, dataManager.getDescription())
                .add(JSON_TOPICS, arrtopic)
                .add(JSON_PREREQUEST, dataManager.getPrerequisites())
                .add(JSON_OUTCOMES, arroutcomes)
                .add(JSON_TEXTBOOKS, arrtextBook)
                .add(JSON_GRADEDCOMPONENT, arrgradingCompnent)
                .add(JSON_GRADINGNOTE,dataManager.getGradingNote())
                .add(JSON_ACADEMIC,dataManager.getAcademicDishonesty())
                .add(JSON_SPECIAL, dataManager.getSpecialAssistance())
                .build();
        
         Map<String, Object> syproperties = new HashMap<>(1);
	syproperties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writersyFactory = Json.createWriterFactory(syproperties);
	StringWriter sysw = new StringWriter();
	JsonWriter jsonsyWriter = writersyFactory.createWriter(sysw);
	jsonsyWriter.writeObject(syllJson);
	jsonsyWriter.close();

	// INIT THE WRITER
	OutputStream syos = new FileOutputStream("./export/sample/SyllabusData.json");
	JsonWriter jsonsyFileWriter = Json.createWriter(syos);
	jsonsyFileWriter.writeObject(syllJson);
	String prettysyPrinted = sysw.toString();
	PrintWriter sypw = new PrintWriter("./export/sample/SyllabusData.json");
	sypw.write(prettysyPrinted);
	sypw.close();
        
        //____________________________________________________________________________
         JsonArrayBuilder HolidayArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder LecturesArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder ReferArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder RecitaArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder HwsArrayBuilder = Json.createArrayBuilder();
        Iterator<ScheduleItem> schIterator = dataManager.SchIterator();
        while (schIterator.hasNext()) {
                ScheduleItem sch = schIterator.next();
                String[] a = sch.getDate().split("/");
                if(sch.getType().equals(JSON_HOLIDAYS)){
                JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, a[0])
                        .add(JSON_DDAAYY, a[1])
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    HolidayArrayBuilder.add(schJson);
                }
                else if(sch.getType().equals("lectures")){
                     JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, a[0])
                        .add(JSON_DDAAYY, a[1])
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    LecturesArrayBuilder.add(schJson);
                }else if(sch.getType().equals("references")){
                    JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, a[0])
                        .add(JSON_DDAAYY, a[1])
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    ReferArrayBuilder.add(schJson);
                }else if(sch.getType().equals(JSON_REEECCCTATION)){
                    JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, a[0])
                        .add(JSON_DDAAYY, a[1])
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    RecitaArrayBuilder.add(schJson);
                }else{
                    JsonObject schJson = Json.createObjectBuilder()
                        .add(JSON_MONTH, a[0])
                        .add(JSON_DDAAYY, a[1])
                        .add(JSON_TITLE, sch.getTitle())
                        .add(JSON_TOPIC,sch.getTopic())
                        .add(JSON_LINK, sch.getLink())
                        .build();
                    HwsArrayBuilder.add(schJson);
                }

	}
	JsonArray HolidayArray = HolidayArrayBuilder.build();
        JsonArray LecturesArray = LecturesArrayBuilder.build();
        JsonArray ReferArray = ReferArrayBuilder.build();
        JsonArray RecitaArray = RecitaArrayBuilder.build();
        JsonArray HwsArray = HwsArrayBuilder.build();
        JsonObject scheJson = Json.createObjectBuilder() 
                .add(JSON_START_MONTH, dataManager.getStartMonth())
                .add(JSON_START_DAY,dataManager.getStartDay())
                .add(JSON_END_MONTH,dataManager.getEndMonth())
                .add(JSON_END_DAY, dataManager.getEndDay())
                .add(JSON_HOLIDAYS,HolidayArray)
                .add(JSON_LECTURE, LecturesArray)
                .add(JSON_REFERENCE, ReferArray)
                .add(JSON_RECITATION,RecitaArray)
		.add(JSON_HWS, HwsArray)
                .build();
        
        Map<String, Object> schproperties = new HashMap<>(1);
	schproperties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerschFactory = Json.createWriterFactory(schproperties);
	StringWriter schsw = new StringWriter();
	JsonWriter jsonschWriter = writerschFactory.createWriter(schsw);
	jsonschWriter.writeObject(scheJson);
	jsonschWriter.close();

	// INIT THE WRITER
	OutputStream schos = new FileOutputStream("./export/sample/ScheduleData.json");
	JsonWriter jsonschFileWriter = Json.createWriter(schos);
	jsonschFileWriter.writeObject(sectionJson);
	String prettyschPrinted = schsw.toString();
	PrintWriter schpw = new PrintWriter("./export/sample/ScheduleData.json");
	schpw.write(prettyschPrinted);
	schpw.close();
        
        String source = "./export/sample";
        File srcDir = new File(source);
        String dest = "./export/"+di[2]+"/"+di[3];
        File destDir = new File(dest);
        FileUtils.copyDirectory(srcDir, destDir);
        
    }
   
}