package cs.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import djf.modules.AppGUIModule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableView;
import cs.CourseSiteGeneratorApp;
import static cs.CourseSitePropertyType.*;
import static cs.CourseSitePropertyType.OH_ALL_RADIO_BUTTON;
import static cs.CourseSitePropertyType.OH_GRAD_RADIO_BUTTON;
import static cs.CourseSitePropertyType.OH_OFFICE_HOURS_TABLE_VIEW;
import static cs.CourseSitePropertyType.OH_TAS_TABLE_VIEW;
import cs.data.TimeSlot.DayOfWeek;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

/**
 * This is the data component for TAManagerApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 */
public class CourseSiteData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    CourseSiteGeneratorApp app;
    
    // THESE ARE ALL THE TEACHING ASSISTANTS
    HashMap<TAType, ArrayList<TeachingAssistantPrototype>> allTAs;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistantPrototype> teachingAssistants;
    
    ObservableList<TimeSlot> officeHours;  
    ArrayList officeHourData = new ArrayList();
    HashMap<String, ArrayList<String>> allSyllabusData;
    
    ObservableList<LecturePropertype> Lectures;
    ObservableList<RecLabPropertype> Recitations;
    ObservableList<RecLabPropertype> Labs;
    
    ObservableList<ScheduleItem> schedule;
    ArrayList scheduleData = new ArrayList();
    
    HashMap<String, ArrayList<String>> allSiteData;
    
    
    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;
    TextArea descriptionTitle;
    TextArea topicTitle;
    TextArea prerequestTitle;
    TextArea outcomesTitle; 
    TextArea textbooksTitle;
    TextArea gradedcomponentTitle;
    TextArea gradednoteTitle;
    TextArea academicTitle;
    TextArea specialTitle;
    ComboBox subject;
    ComboBox number;
    ComboBox semester;
    ComboBox year;
    TextField title;
    CheckBox Home;
    CheckBox Syllabus;
    CheckBox Schedule;
    CheckBox HWs;
    ImageView favicon;
    ImageView navbar;
    ImageView Left;
    ImageView Right;
    TextField InstructorName;
    TextField InstructorLink;
    TextField InstructorEmail;
    TextField InstructorRoom;
    TextArea Instructoroh;
    String pathForFav;
    String pathForNav;
    String pathForLeft;
    String pathForRight;
    DatePicker date1;
    DatePicker date2;
    Text export;
    ComboBox style;
   
    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public CourseSiteData(CourseSiteGeneratorApp initApp){
        
            // KEEP THIS FOR LATER
            app = initApp;
            AppGUIModule gui = app.getGUIModule();
            pathForFav = "./images/sblogo.png";
            pathForNav ="./images/SBUDarkRed.png";
            pathForLeft = "./images/SBUWhite.jpg";
            pathForRight = "./images/SBUCSLogo.png";
            
            // SETUP THE DATA STRUCTURES
            allTAs = new HashMap();
            allTAs.put(TAType.Graduate, new ArrayList());
            allTAs.put(TAType.Undergraduate, new ArrayList());
            
//            allSyllabusData = new HashMap();
//            allSyllabusData.put("description", new ArrayList());
//            allSyllabusData.put("topics", new ArrayList());
//            allSyllabusData.put("prerequisites", new ArrayList());
//            allSyllabusData.put("outcomes", new ArrayList());
//            allSyllabusData.put("textbooks", new ArrayList());
//            allSyllabusData.put("gradedComponents", new ArrayList());
//            allSyllabusData.put("gradingNote", new ArrayList());
//            allSyllabusData.put("academicDishonesty", new ArrayList());
//            allSyllabusData.put("specialAssistance", new ArrayList());
            
            allSiteData = new HashMap();
            allSiteData.put("subject",new ArrayList());
            allSiteData.put("number", new ArrayList());
            allSiteData.put("semester", new ArrayList());
            
         
            
            // GET THE LIST OF TAs FOR THE LEFT TABLE
            TableView<TeachingAssistantPrototype> taTableView = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
            teachingAssistants = taTableView.getItems();
            
            descriptionTitle = (TextArea)gui.getGUINode(CS_SYLL_DES_TA_PANE);
            topicTitle = (TextArea)gui.getGUINode(CS_STYLL_TOPIC_TA);
            prerequestTitle = (TextArea)gui.getGUINode(CS_STYLL_PREREQUIST_TA);
            outcomesTitle = (TextArea)gui.getGUINode(CS_STYLL_OUTCOME_TA);
            textbooksTitle = (TextArea)gui.getGUINode(CS_STYLL_TEXTBOOK_TA);
            gradedcomponentTitle = (TextArea)gui.getGUINode(CS_STYLL_GRADEDCOMP_TA);
            gradednoteTitle = (TextArea)gui.getGUINode(CS_STYLL_GRADENOTE_TA);
            academicTitle = (TextArea)gui.getGUINode(CS_STYLL_ACADEMIC_TITLE_TA);
            specialTitle = (TextArea)gui.getGUINode(CS_STYLL_SPEASS_TITLE_TA);
            //Get things from site pane
            subject = (ComboBox)gui.getGUINode(CS_SITE_SUBJECT_COMBOBOX);
            number  =(ComboBox)gui.getGUINode(CS_SITE_NUMBER_COMBOBOX);
            semester =(ComboBox)gui.getGUINode(CS_SITE_SEMESTER_COMBOBOX);
            year = (ComboBox)gui.getGUINode(CS_SITE_YEAR_COMBOBOX);
            style = ((ComboBox)gui.getGUINode(SITE_STYLE_COMBO));
            title = ((TextField)gui.getGUINode(CS_SITE_TITLE_TF));
            
            Home = ((CheckBox)gui.getGUINode(CS_SITE_HOME_CHECKBOX));
            Syllabus = ((CheckBox)gui.getGUINode(CS_SITE_SYLLABUS_CHECKBOX));
            Schedule = ((CheckBox)gui.getGUINode(CS_SITE_SCHEDULE_CHECKBOX));
            HWs = ((CheckBox)gui.getGUINode(CS_SITE_HWS_CHECKBOX));
            
            favicon = ((ImageView)gui.getGUINode(SITE_FAV_ICON));
            navbar = ((ImageView)gui.getGUINode(SITE_STYLE_ICON));
            Left =((ImageView)gui.getGUINode(SITE_STYLE_LEFT_ICON));
            Right=((ImageView)gui.getGUINode(SITE_STYLE_RIGHT_ICON));
            
            InstructorName = ((TextField)gui.getGUINode(CS_SITE_NAME_TEXTFIELD));
            InstructorLink = ((TextField)gui.getGUINode(CS_SITE_HOMEPAGE_TEXTFIELD));
            InstructorEmail  = ((TextField)gui.getGUINode(CS_SITE_EMAIL_TEXTFIELD));
            InstructorRoom  = ((TextField)gui.getGUINode(CS_SITE_ROOM_TEXTFIELD));
            Instructoroh =((TextArea)gui.getGUINode(CS_SITE_OFFICE_TA_PANE));
            
            TableView lecTable =(TableView)gui.getGUINode(CS_MT_LEC_TABLE_VIEW);
            Lectures = lecTable.getItems();
            TableView labTable = (TableView)gui.getGUINode(CS_MT_LAB_TABLE_VIEW);
            Labs = labTable.getItems();
            TableView recTable = (TableView)gui.getGUINode(CS_MT_REC_TABLE_VIEW);
            Recitations = recTable.getItems();
            TableView schTable = (TableView)gui.getGUINode(CS_SCH_TABLE_VIEW);
            schedule = schTable.getItems();
            
            date1 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER));
            date2 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER2));
            
            export = ((Text) gui.getGUINode(CS_SITE_EXPOR_TEXT));
            // THESE ARE THE DEFAULT OFFICE HOURS
            startHour = MIN_START_HOUR;
            endHour = MAX_END_HOUR;
            
            resetOfficeHours();
            resetSyllubus();
            year.getSelectionModel().selectFirst();
            semester.getSelectionModel().selectFirst();
            
    }
    
    // ACCESSOR METHODS
    public String getDatePicker(){
        LocalDate a = date1.getValue();
        String b;
        if(a!= null){
        b =a.toString();
        }else{
            b = "";
        }
        return b ;
    }
    public String getStartMonth(){
        LocalDate a = date1.getValue();
        String b;
        String startmonth;
        if(a!= null){
        b =a.toString();
        String[] c = b.split("-");
        startmonth = c[1];
        }else{
            b = "";
            startmonth = "";
        }
        return startmonth;
    }
    public String getStartDay(){
         LocalDate a = date1.getValue();
        String b;
        String startday;
        if(a!= null){
        b =a.toString();
        String[] c = b.split("-");
        startday = c[2];
        }else{
            b = "";
            startday = "";
        }
        return startday;
    
    }
    public String getEndMonth(){
        LocalDate a = date2.getValue();
        String b;
        String startmonth;
        if(a!= null){
        b =a.toString();
        String[] c = b.split("-");
        startmonth = c[1];
        }else{
            b = "";
            startmonth = "";
        }
        return startmonth;
    }
    public String getEndDay(){
         LocalDate a = date2.getValue();
        String b;
        String startday;
        if(a!= null){
        b =a.toString();
        String[] c = b.split("-");
        startday = c[2];
        }else{
            b = "";
            startday = "";
        }
        return startday;
    
    }
    public String getEndDate(){
        LocalDate a = date2.getValue();
        String b;
        if(a!= null){
        b =a.toString();
        }else{
            b = "";
        }
        return b;  
    }

    public int getStartHour() {
        return startHour;
    }
    public int getEndHour() {
        return endHour;
    }
    public String getSubject(){
        return subject.getSelectionModel().getSelectedItem().toString();
    }
    public String getNumber(){
        return number.getSelectionModel().getSelectedItem().toString();
    }
    public String getSemester(){
        return semester.getSelectionModel().getSelectedItem().toString();
    }
    public String getYear(){
        return year.getSelectionModel().getSelectedItem().toString();
    }
    public String getTitle(){
        return title.getText();
    }
    public String getImagePath(String type){
        switch(type){
            case "Fav":
                return pathForFav;
            case "Nav":
                return pathForNav;
            case "Left":
                return pathForLeft;
            case "Right":
                return pathForRight;
        }
        return null;
    }
    public String getInstruname(){
        return InstructorName.getText();
    }
    public String getInstrulink(){
        return InstructorLink.getText();
    }
    public String getInstruemail(){
        return InstructorEmail.getText();
    }
    public String getInstruroom(){
        return InstructorRoom.getText();
    }
    public String getInstruhours(){
        return Instructoroh.getText();
    }
    public void setHome(Boolean a){
        Home.setSelected(a);
    }
    public void setSyllabus(Boolean a){
        Syllabus.setSelected(a);
    }
    public void setSchedule(Boolean a){
        Schedule.setSelected(a);
    }
    public void setHWs(Boolean a){
        HWs.setSelected(a);
    }
    public boolean getHome(){
        if(Home.isSelected()){
            return true;
        }else{
            return false;
        }
    }
    public boolean getSyllabus(){
        if(Syllabus.isSelected()){
            return true;
        }else{
            return false;
        }
    }
    public boolean getSchedule(){
        if(Schedule.isSelected()){
            return true;
        }else{
            return false;
        }
    }
    public boolean getHWs(){
        if(HWs.isSelected()){
            return true;
        }else{
            return false;
        }
    }
    public String getDescription(){
        return descriptionTitle.getText();
    }
    public String getTopic(){
        return topicTitle.getText();
    }
    public String getPrerequisites(){
        return prerequestTitle.getText();
    }
    public String getOutcomes(){
        return outcomesTitle.getText();
    }
    public String getTextbooks(){
        return textbooksTitle.getText();
    }
    public String getGradedComponents(){
        return gradedcomponentTitle.getText();
    }
    public String getGradingNote(){
        return gradednoteTitle.getText();
    }
    public String getAcademicDishonesty(){
        return academicTitle.getText();
    }
    public String getSpecialAssistance(){
        return specialTitle.getText();
    }
   
    // PRIVATE HELPER METHODS
    
    private void sortTAs() {
        Collections.sort(teachingAssistants);
    }
     public String getStringFav(){
        return pathForFav;
    }
    public void setStringFav(String path){
        pathForFav = path;
    }
    public void setNav(String path){
        pathForNav = path;
    }
    public void setLeft(String path){
        pathForLeft = path;
    }
    public void setRight(String path){
        pathForRight = path;
    }
    
    private void resetSyllubus(){
        descriptionTitle.clear();
        topicTitle.clear();
        prerequestTitle.clear();
        outcomesTitle.clear();
        textbooksTitle.clear();
        gradedcomponentTitle.clear();
        gradednoteTitle.clear();
        academicTitle.clear();
        specialTitle.clear();
    }
    public void resetInstructor(){
        InstructorName.clear();
        InstructorLink.clear();
        InstructorEmail.clear();
        InstructorRoom.clear();
        Instructoroh.clear();
    }
    private void resetOfficeHours() {
        //THIS WILL STORE OUR OFFICE HOURS
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView)gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        
        
        officeHours = officeHoursTableView.getItems(); 
        officeHours.clear();
        
        for (int i = startHour; i <= endHour; i++) {
            TimeSlot timeSlot = new TimeSlot(   this.getTimeString(i, true),
                                                this.getTimeString(i, false));
            officeHours.add(timeSlot);
            officeHourData.add(timeSlot);
            
            TimeSlot halfTimeSlot = new TimeSlot(   this.getTimeString(i, false),
                                                    this.getTimeString(i+1, true));
            officeHours.add(halfTimeSlot);
            officeHourData.add(halfTimeSlot);
        }
        
    }
    
    private String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    // METHODS TO OVERRIDE
        
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void reset() {
        try {
            AppGUIModule gui = app.getGUIModule();
            startHour = MIN_START_HOUR;
            endHour = MAX_END_HOUR;
            teachingAssistants.clear();
            for (TimeSlot timeSlot : officeHours) {
                timeSlot.reset();
            }
            resetSyllubus();
            allTAs.get(TAType.Graduate).clear();
            allTAs.get(TAType.Undergraduate).clear();
             
            officeHourData.clear();
            Lectures.clear();
            Recitations.clear();
            Labs.clear();
            schedule.clear();
            scheduleData.clear();
            date1.setValue(null);
            date2.setValue(null);
            year.getSelectionModel().selectFirst();
            semester.getSelectionModel().selectFirst();
            ComboBox Subject_combo = (ComboBox)gui.getGUINode(CS_SITE_SUBJECT_COMBOBOX);
            Subject_combo.getItems().clear();
            ComboBox Number_combo = (ComboBox)gui.getGUINode(CS_SITE_NUMBER_COMBOBOX);
            Number_combo.getItems().clear();
            allSiteData.get("subject").clear();
            allSiteData.get("number").clear();
            
            File file = new File("./work/subject.txt");
            Scanner sc = new Scanner(file);
            ArrayList sublist = allSiteData.get("subject");
           
            while (sc.hasNextLine()){
                String a = sc.nextLine();
                sublist.add(a);
               
            }
           //System.out.print(j);
            
            for(int i = 0; i <sublist.size(); i++){
                Subject_combo.getItems().add(sublist.get(i));
            }
            Subject_combo.getSelectionModel().selectFirst();
            
            File numFile = new File("./work/number.txt");
            Scanner numsc = new Scanner(numFile);
            
            ArrayList numlist = allSiteData.get("number");
            while(numsc.hasNextLine()){
                String b = numsc.nextLine();
                numlist.add(b);
            }
           
            for(int i = 0; i <numlist.size(); i++){
                Number_combo.getItems().add(numlist.get(i));
            }
            Number_combo.getSelectionModel().selectFirst();
            resetImage();
            resetInstructor();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CourseSiteData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CourseSiteData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
      title.clear();
    }
    
    // SERVICE METHODS
    
    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if (initStartHour <= initEndHour) {
            // THESE ARE VALID HOURS SO KEEP THEM
            // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
            startHour = initStartHour;
            endHour = initEndHour;
        }
        resetOfficeHours();
    }
   
    public void addToLecture(LecturePropertype le){
        Lectures.add(le);   
       
    }
    public void removeFromLecture(LecturePropertype le){
        Lectures.remove(le);
    }
    public void addToLab(RecLabPropertype la){
        Labs.add(la);
    }
    public void removeFromLab(RecLabPropertype la){
        Labs.remove(la);
    }
    public void addToRec(RecLabPropertype re){
        Recitations.add(re);
    }
    public void removeFromRec(RecLabPropertype re){
        Recitations.remove(re);
    }
    public void addToSch(ScheduleItem sch){
        schedule.add(sch);
        scheduleData.add(sch);
    }
    public void removeFromSch(ScheduleItem sch){
        schedule.remove(sch);
        scheduleData.remove(sch);
    }
    public ArrayList getSchData(){
        return scheduleData;
    }
    public String getExpor(){
        return export.getText();
    }
    public void addToSite(String str, String type){
        String Number = "";
        String Semester = "";
        String Year = "";
        String Subject = "";
        String Style = "";
        switch(type){
            case "subject":
                subject.getSelectionModel().select(str);
                Number = number.getSelectionModel().getSelectedItem().toString();
                Semester = semester.getSelectionModel().getSelectedItem().toString();
                Year = year.getSelectionModel().getSelectedItem().toString();
                export.setText("./export/" + str + "_" + Number + "_" + Semester + "_" + Year + "/public_html");
                break;
            case "number":
                Subject = subject.getSelectionModel().getSelectedItem().toString();
                Semester = semester.getSelectionModel().getSelectedItem().toString();
                Year = year.getSelectionModel().getSelectedItem().toString();
                export.setText("./export/" + Subject + "_" + str + "_" + Semester + "_" + Year + "/public_html");
                number.getSelectionModel().select(str);
                break;
            case "semester":
                Subject = subject.getSelectionModel().getSelectedItem().toString();
                Number = number.getSelectionModel().getSelectedItem().toString();
                Year = year.getSelectionModel().getSelectedItem().toString();
                export.setText("./export/" + Subject + "_" + Number + "_" + str + "_" + Year + "/public_html");
                semester.getSelectionModel().select(str);
                break;
            case "year":
                Subject = subject.getSelectionModel().getSelectedItem().toString();
                Number = number.getSelectionModel().getSelectedItem().toString();
                Semester = semester.getSelectionModel().getSelectedItem().toString();
                export.setText("./export/" + Subject + "_" + Number + "_" + Semester + "_" + str + "/public_html");
                year.getSelectionModel().select(str);
                break;
            case "title":
                title.setText(str);
                break;
            case "style":
                style.getSelectionModel().select(str);
                break;
                
        }  
    }
    public void addPageClick(String str){
        switch(str){
            case "Home":
                Home.setSelected(true);
                break;
            case "Syllabus":
                Syllabus.setSelected(true);
                break;
            case "Schedule":
                Schedule.setSelected(true);
                break;
            case "HWs":
                HWs.setSelected(true);
                break;
        }
        
    }
    public void resetImage(){
        Image imagefav = new Image("file:"+pathForFav);
        favicon.setImage(imagefav);
        Image imagenav = new Image("file:"+pathForNav);
        navbar.setImage(imagenav);
        Image imageLeft = new Image("file:"+pathForLeft);
        Left.setImage(imageLeft);
        Image imageRight = new Image("file:"+pathForRight);
        Right.setImage(imageRight);
    }
    public void addImage(String des,String imagePath){
        Image image = new Image("file:"+imagePath);
        switch(des){
            case"favicon": 
                favicon.setImage(image);
                break;
            case"navbar":
                navbar.setImage(image);
                break;
            case"Left":
                //image = new Image("file:"+imagePath);
                Left.setImage(image);
                break;
            case"Right":
               // image = new Image()
                Right.setImage(image);
                break;
                
        }
    }
    public void addInstructor(String des, String text){
        switch(des){
            case"name":
                InstructorName.setText(text);
                break;
            case"homepage":
                InstructorLink.setText(text);
                break;
            case"room":
                InstructorRoom.setText(text);
                break;
            case"email":
                InstructorEmail.setText(text);
                break;
            case"oh":
                Instructoroh.setText(text);
                break;
        }
        
    }
    public void addToDatePicker(String dateString, String type){
        if(!dateString.equals("") ){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(dateString, formatter);
         switch(type){
            case "start":
                date1.setValue(localDate);
                break;
            case "end":
                date2.setValue(localDate);
                break;
         }
        }else{
            switch(type){
            case "start":
                date1.setValue(null);
                break;
            case "end":
                date2.setValue(null);
                break; 
            
        }
        }
        
    }

    public void addToSyllubus(String str,String type){
        
//        ArrayList<String> SyllabusThing = allSyllabusData.get(type);
//        SyllabusThing.add(str);
        switch(type){
            case "description":
                descriptionTitle.clear();
                //需要以后把每一个str的东西放进去
                descriptionTitle.setText(str);
                break;
            case "topics":
                topicTitle.clear();
                topicTitle.setText(str);
                break;
            case "prerequisites":
                 prerequestTitle.clear();
                prerequestTitle.setText(str);
                break;
            case "outcomes":
                outcomesTitle.clear();
                outcomesTitle.setText(str);
                break;
            case "textbooks":
                textbooksTitle.clear();
                textbooksTitle.setText(str);
                break;
            case "gradedComponents":
                gradedcomponentTitle.clear();
                gradedcomponentTitle.setText(str);
                break;
            case "gradingNote":
                gradednoteTitle.clear();
                gradednoteTitle.setText(str);
                break;
            case "academicDishonesty":
                academicTitle.clear();
                academicTitle.setText(str);
                break;
            case "specialAssistance":
                specialTitle.clear();
                specialTitle.setText(str);
                break;
                
            
        }
        
    }
    /*
    public void removeFromSyllubus(String str, String type){
          switch(type){
            case "description":
                String a = descriptionTitle.getText();
                a = a - str;
                descriptionTitle.setText(a);
          }
    }
    */
    public void addNewToSyllubus(String str,String type){
        
        ArrayList<String> SyllabusThing = allSyllabusData.get(type);
        SyllabusThing.add(str);        
        switch(type){
            case "description":
                //String a = descriptionTitle.getText();
                //a = a + str;
                descriptionTitle.setText(str);
            case "topics":
                topicTitle.clear();
                topicTitle.setText(str);
            case "prerequisites":
                 prerequestTitle.clear();
                prerequestTitle.setText(str);
            case "outcomes":
                outcomesTitle.clear();
                outcomesTitle.setText(str);
            case "textbooks":
                textbooksTitle.clear();
                textbooksTitle.setText(str);
            case "gradedComponents":
                gradedcomponentTitle.clear();
                gradedcomponentTitle.setText(str);
            case "gradingNote":
                gradednoteTitle.clear();
                gradednoteTitle.setText(str);
            case "academicDishonesty":
                academicTitle.clear();
                academicTitle.setText(str);
            case "specialAssistance":
                specialTitle.clear();
                specialTitle.setText(str);
                
            
        }
        
    }
    public void addTA(TeachingAssistantPrototype ta) {
        if (!hasTA(ta)) {
            TAType taType = TAType.valueOf(ta.getType());
            ArrayList<TeachingAssistantPrototype> tas = allTAs.get(taType);
            tas.add(ta);
            this.updateTAs();
        }
    }

    public void addTA(TeachingAssistantPrototype ta, HashMap<TimeSlot, ArrayList<DayOfWeek>> officeHours) {
        addTA(ta);
        for (TimeSlot timeSlot : officeHours.keySet()) {
            ArrayList<DayOfWeek> days = officeHours.get(timeSlot);
            for (DayOfWeek dow : days) {
                timeSlot.addTA(dow, ta);
            }
        }
    }
    
    public void removeTA(TeachingAssistantPrototype ta) {
        // REMOVE THE TA FROM THE LIST OF TAs
        //TAType taType = TAType.valueOf(ta.getType());
        //allTAs.get(taType).remove(ta);
        allTAs.get(TAType.valueOf("Undergraduate")).remove(ta);
        allTAs.get(TAType.valueOf("Graduate")).remove(ta);
        // REMOVE THE TA FROM ALL OF THEIR OFFICE HOURS
        for (TimeSlot timeSlot : officeHours) {
            timeSlot.removeTA(ta);
        }
        
        // AND REFRESH THE TABLES
        this.updateTAs();
    }

    public void removeTA(TeachingAssistantPrototype ta, HashMap<TimeSlot, ArrayList<DayOfWeek>> officeHours) {
        removeTA(ta);
        for (TimeSlot timeSlot : officeHours.keySet()) {
            ArrayList<DayOfWeek> days = officeHours.get(timeSlot);
            for (DayOfWeek dow : days) {
                timeSlot.removeTA(dow, ta);
            }
        }    
    }
    
    public DayOfWeek getColumnDayOfWeek(int columnNumber) {
        return TimeSlot.DayOfWeek.values()[columnNumber-2];
    }

    public TeachingAssistantPrototype getTAWithName(String name) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getName().equals(name))
                return ta;
        }
        return null;
    }

    public TeachingAssistantPrototype getTAWithEmail(String email) {
        Iterator<TeachingAssistantPrototype> taIterator = teachingAssistants.iterator();
        while (taIterator.hasNext()) {
            TeachingAssistantPrototype ta = taIterator.next();
            if (ta.getEmail().equals(email))
                return ta;
        }
        return null;
    }
    public TimeSlot getTimeSlotnew(String startTime){
        Iterator<TimeSlot> Iterator = officeHourData.iterator();
        for(int i = 0; i < officeHourData.size(); i++){
            String timeSlotStartTime =((TimeSlot) officeHourData.get(i)).getStartTime();
            System.out.print(timeSlotStartTime);
            if(timeSlotStartTime.equals(startTime)){
                System.out.print("++++++++++++++++++++++++");
                return (TimeSlot) officeHourData.get(i);
            }else{
                continue;
            }
        }
        /*
        while (Iterator.hasNext()) {
            TimeSlot timeSlot = Iterator.next();
            String timeSlotStartTime = timeSlot.getStartTime().replace(":", "_");
            System.out.print(timeSlotStartTime);
            if (timeSlotStartTime.equals(startTime))
                System.out.print("++++++++++++++++++++++++");
                return timeSlot;
        }
*/
        return null;
    }
    public TimeSlot getTimeSlot(String startTime) {
        Iterator<TimeSlot> timeSlotsIterator = officeHours.iterator();
        while (timeSlotsIterator.hasNext()) {
            TimeSlot timeSlot = timeSlotsIterator.next();
            String timeSlotStartTime = timeSlot.getStartTime().replace(":", "_");
            if (timeSlotStartTime.equals(startTime))
                return timeSlot;
        }
        return null;
    }

    public TAType getSelectedType() {
        RadioButton allRadio = (RadioButton)app.getGUIModule().getGUINode(OH_ALL_RADIO_BUTTON);
        if (allRadio.isSelected())
            return TAType.All;
        RadioButton gradRadio = (RadioButton)app.getGUIModule().getGUINode(OH_GRAD_RADIO_BUTTON);
        if (gradRadio.isSelected())
            return TAType.Graduate;
        else
            return TAType.Undergraduate;
    }

    public TeachingAssistantPrototype getSelectedTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TeachingAssistantPrototype> tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        return tasTable.getSelectionModel().getSelectedItem();
    }
    
    public HashMap<TimeSlot, ArrayList<DayOfWeek>> getTATimeSlots(TeachingAssistantPrototype ta) {
        HashMap<TimeSlot, ArrayList<DayOfWeek>> timeSlots = new HashMap();
        for (TimeSlot timeSlot : officeHours) {
            if (timeSlot.hasTA(ta)) {
                ArrayList<DayOfWeek> daysForTA = timeSlot.getDaysForTA(ta);
                timeSlots.put(timeSlot, daysForTA);
            }
        }
        return timeSlots;
    }
    
    private boolean hasTA(TeachingAssistantPrototype testTA) {
        return allTAs.get(TAType.Graduate).contains(testTA)
                ||
                allTAs.get(TAType.Undergraduate).contains(testTA);
    }
    
    public boolean isTASelected() {
        AppGUIModule gui = app.getGUIModule();
        TableView tasTable = (TableView)gui.getGUINode(OH_TAS_TABLE_VIEW);
        return tasTable.getSelectionModel().getSelectedItem() != null;
    }

    public boolean isLegalNewTA(String name, String email) {
        if ((name.trim().length() > 0)
                && (email.trim().length() > 0)) {
            // MAKE SURE NO TA ALREADY HAS THE SAME NAME
            TAType type = this.getSelectedType();
            TeachingAssistantPrototype testTA = new TeachingAssistantPrototype(name, email, type);
            if (this.teachingAssistants.contains(testTA))
                return false;
            if (this.isLegalNewEmail(email)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isLegalNewName(String testName) {
        if (testName.trim().length() > 0) {
            for (TeachingAssistantPrototype testTA : this.teachingAssistants) {
                if (testTA.getName().equals(testName))
                    return false;
            }
            return true;
        }
        return false;
    }
    
    public boolean isLegalNewEmail(String email) {
        Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
                "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);
        if (matcher.find()) {
            for (TeachingAssistantPrototype ta : this.teachingAssistants) {
                if (ta.getEmail().equals(email.trim()))
                    return false;
            }
            return true;
        }
        else return false;
    }
    
    public boolean isDayOfWeekColumn(int columnNumber) {
        return columnNumber >= 2;
    }
    
    public boolean isTATypeSelected() {
        AppGUIModule gui = app.getGUIModule();
        RadioButton allRadioButton = (RadioButton)gui.getGUINode(OH_ALL_RADIO_BUTTON);
        return !allRadioButton.isSelected();
    }
    
    public boolean isValidTAEdit(TeachingAssistantPrototype taToEdit, String name, String email) {
        if (!taToEdit.getName().equals(name)) {
            if (!this.isLegalNewName(name))
                return false;
        }
        if (!taToEdit.getEmail().equals(email)) {
            if (!this.isLegalNewEmail(email))
                return false;
        }
        return true;
    }

    public boolean isValidNameEdit(TeachingAssistantPrototype taToEdit, String name) {
        if (!taToEdit.getName().equals(name)) {
            if (!this.isLegalNewName(name))
                return false;
        }
        return true;
    }

    public boolean isValidEmailEdit(TeachingAssistantPrototype taToEdit, String email) {
        if (!taToEdit.getEmail().equals(email)) {
            if (!this.isLegalNewEmail(email))
                return false;
        }
        return true;
    }    

    public void updateTAs() {
        TAType type = getSelectedType();
        selectTAs(type);
    }
    
    public void selectTAs(TAType type) {
        teachingAssistants.clear();
        Iterator<TeachingAssistantPrototype> tasIt = this.teachingAssistantsIterator();
        while (tasIt.hasNext()) {
            TeachingAssistantPrototype ta = tasIt.next();
            if (type.equals(TAType.All)) {
                teachingAssistants.add(ta);
            }
            else if (ta.getType().equals(type.toString())) {
                teachingAssistants.add(ta);
            }
        }
        
        // SORT THEM BY NAME
        sortTAs();

        // CLEAR ALL THE OFFICE HOURS
        Iterator<TimeSlot> officeHoursIt = officeHours.iterator();
        while (officeHoursIt.hasNext()) {
            TimeSlot timeSlot = officeHoursIt.next();
            timeSlot.filter(type);
        }
        
        app.getFoolproofModule().updateAll();
    }
    
    public Iterator<TimeSlot> officeHoursIterator() {
        return officeHours.iterator();
    }
    public Iterator<TimeSlot> officeDataIterator(){
        return officeHourData.iterator();
    }
    public Iterator<TeachingAssistantPrototype> teachingAssistantsIterator() {
        return new AllTAsIterator();
    }
    public Iterator<LecturePropertype> LectureIterator(){
        return Lectures.iterator();
    }
    public Iterator<RecLabPropertype> RecIterator(){
        return Recitations.iterator();
    }
    public Iterator<RecLabPropertype> LabIterator(){
        return Labs.iterator();
    }
    public Iterator<ScheduleItem> SchIterator(){
        return schedule.iterator();
    }
    public ArrayList getOfficeHourData(){
        return officeHourData;
    } 
   
    public ObservableList<TimeSlot> getObservableList(){
        return officeHours;
    }
    public ObservableList<ScheduleItem> getSchOL(){
        return schedule;
    }
    public HashMap<String, ArrayList<String>>getAllSiteData(){
        return allSiteData;
    }
    private class AllTAsIterator implements Iterator {
        Iterator gradIt = allTAs.get(TAType.Graduate).iterator();
        Iterator undergradIt = allTAs.get(TAType.Undergraduate).iterator();

        public AllTAsIterator() {}
       
        @Override
        public boolean hasNext() {
            if (gradIt.hasNext() || undergradIt.hasNext())
                return true;
            else
                return false;                
        }

        @Override
        public Object next() {
            if (gradIt.hasNext())
                return gradIt.next();
            else if (undergradIt.hasNext())
                return undergradIt.next();
            else
                return null;
        }
       
    }
}