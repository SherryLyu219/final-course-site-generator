/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author lvxinrui
 */
public class RecLabPropertype {
    private final StringProperty section;
    private final StringProperty day_time;
    private final StringProperty room;
    private final StringProperty ta1;
    private final StringProperty ta2;
    
    public RecLabPropertype(String initSection, String initday_time,String initRoom,String initTa1, String initTa2){
        section = new SimpleStringProperty(initSection);
        day_time = new SimpleStringProperty(initday_time);
        room = new SimpleStringProperty(initRoom);
        ta1 = new SimpleStringProperty(initTa1);
        ta2 = new SimpleStringProperty(initTa2);
    }
    public String getSection(){
        return section.get();
    }
    public void setSection(String initSection){
         section.set(initSection);
     }
    public String getDay_time(){
        return day_time.get();
    }
    public void setDay_Time(String initdayTime){
         day_time.set(initdayTime);
     }
    public String getRoom(){
        return room.get();
    }
    public void setRoom(String initroom){
         room.set(initroom);
     }
    public String getTa1(){
        return ta1.get();
    }
    public void setTa1(String initta1){
         ta1.set(initta1);
     }
    public String getTa2(){
        return ta2.get();
    }
     public void setTa2(String initta2){
         ta2.set(initta2);
     }
    
}
