/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class EditCheckBox_Transaction implements jTPS_Transaction{
    CourseSiteData data;
    String type;
    Boolean oldValue;
    Boolean newValue;
    
    public EditCheckBox_Transaction(CourseSiteData initData,String initType, Boolean initOld, Boolean initNew) {
        data = initData;
        type = initType;
        oldValue = initOld;
        newValue = initNew;
    }

    @Override
    public void doTransaction() {
        switch(type){
            case"home":
                data.setHome(newValue);
                break;
            case"syllabus":
                data.setSyllabus(newValue);
                break;
            case"schedule":
                data.setSchedule(newValue);
                break;
            case"hws":
                data.setHWs(newValue);
                break;
        }
        
             
    }

    @Override
    public void undoTransaction() {
        switch(type){
            case"home":
                data.setHome(oldValue);
                break;
            case"syllabus":
                data.setSyllabus(oldValue);
                break;
            case"schedule":
                data.setSchedule(oldValue);
                break;
            case"hws":
                data.setHWs(oldValue);
                break;
        }
       
    } 
    
}
