package cs.transactions;

import jtps.jTPS_Transaction;
import cs.CourseSiteGeneratorApp;
import cs.data.CourseSiteData;
import cs.data.TeachingAssistantPrototype;

public class PasteTA_Transaction implements jTPS_Transaction {
    CourseSiteGeneratorApp app;
    TeachingAssistantPrototype taToPaste;

    public PasteTA_Transaction(  CourseSiteGeneratorApp initApp, 
                                 TeachingAssistantPrototype initTAToPaste) {
        app = initApp;
        taToPaste = initTAToPaste;
    }

    @Override
    public void doTransaction() {
        CourseSiteData data = (CourseSiteData)app.getDataComponent();
        data.addTA(taToPaste);
    }

    @Override
    public void undoTransaction() {
        CourseSiteData data = (CourseSiteData)app.getDataComponent();
        data.removeTA(taToPaste);
    }   
}