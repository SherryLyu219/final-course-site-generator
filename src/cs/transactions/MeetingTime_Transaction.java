/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import cs.data.LecturePropertype;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class MeetingTime_Transaction implements jTPS_Transaction{
    LecturePropertype le;
    String propertype;
    String oldValue;
    String newValue;
    TableView ta;
    
    public MeetingTime_Transaction(TableView initTa,String initpro, LecturePropertype initle, String initOld, String initNew) {
        le = initle;
        propertype = initpro;
        oldValue = initOld;
        newValue = initNew;
        ta = initTa;
    }

    @Override
    public void doTransaction() {
        switch(propertype){
            case"section":
               le.setSection(newValue);
               ta.refresh(); 
               break;
            case"days":
                le.setDay(newValue);
                ta.refresh();
                break;
            case"time":
               // System.out.print(le.getRoom());
                le.setTime(newValue);
                //System.out.print(le.getRoom());
                ta.refresh();
                break; 
            case"room":
                le.setRoom(newValue);
                ta.refresh();
                break;
              
        }
       
        //le.setSectionProperty(newValue);

             
    }

    @Override
    public void undoTransaction() {
        switch(propertype){
            case"section":
                le.setSection(oldValue);
                ta.refresh();
                break;
            case"days":
                le.setDay(oldValue);
                ta.refresh();
                break; 
            case"time":
                le.setTime(oldValue);
                ta.refresh();
                break;
            case"room":
                le.setRoom(oldValue);
                ta.refresh();
                break;
        }
        
        
        
        //le.setSectionProperty(oldValue);

       
    }  
}
