/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import cs.data.RecLabPropertype;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class CutRecLab_Transaction implements jTPS_Transaction{
     RecLabPropertype leToCut;
     CourseSiteData data;
     String pro;
    public CutRecLab_Transaction(CourseSiteData initData, RecLabPropertype initleToCut, String initpro) {
        data = initData;
        leToCut = initleToCut;
        pro = initpro;
    }
     @Override
    public void doTransaction() {
        switch(pro){
            case"lab":
                data.removeFromLab(leToCut);
                break;
            case"rec":
                data.removeFromRec(leToCut);
                break;
        }
//        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }

    @Override
    public void undoTransaction() {
          switch(pro){
            case"lab":
                data.addToLab(leToCut);
                break;
            case"rec":
                data.addToRec(leToCut);
                break;
        }
    }   
    
}
