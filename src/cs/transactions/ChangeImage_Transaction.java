/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class ChangeImage_Transaction implements jTPS_Transaction {
     CourseSiteData data;
     ImageView Im;
     String oldFilePath;
     String newFilePath;
     String type;
     
     
      public ChangeImage_Transaction(CourseSiteData initdata,ImageView initIm, String initold, String initnew,String inittype) {
        data = initdata;
        Im = initIm;
        oldFilePath = initold;
        newFilePath = initnew;
        type = inittype;
    }
      @Override
    public void doTransaction() { 
        Image image1 = new Image("file:"+newFilePath); 
        Im.setImage(image1);
        switch(type){
            case"fav":
                data.setStringFav(newFilePath);
                break;
            case"nav":
                data.setNav(newFilePath);
                break;
            case"left":
                data.setLeft(newFilePath);
                break;
            case"right":
                data.setRight(newFilePath);
                break;
  
        }

    }
     @Override
     public void undoTransaction(){
        Image image1 = new Image("file:"+oldFilePath);
        Im.setImage(image1);
         switch(type){
            case"fav":
                data.setStringFav(oldFilePath);
                break;
            case"nav":
                data.setNav(oldFilePath);
                break;
            case"left":
                data.setLeft(oldFilePath);
                break;
            case"right":
                data.setRight(oldFilePath);
                break;             
        }
     }
}
