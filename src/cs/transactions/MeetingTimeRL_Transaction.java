/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.RecLabPropertype;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class MeetingTimeRL_Transaction implements jTPS_Transaction{
    RecLabPropertype rl;
    String propertype;
    String oldValue;
    String newValue;
    TableView ta;
    
    public MeetingTimeRL_Transaction(TableView initTa,String initpro, RecLabPropertype initrl, String initOld, String initNew) {
        rl = initrl;
        propertype = initpro;
        oldValue = initOld;
        newValue = initNew;
        ta = initTa;
    }

    @Override
    public void doTransaction() {
        switch(propertype){
            case"section":
               rl.setSection(newValue);
               ta.refresh(); 
               break;
            case"daytime":
                rl.setDay_Time(newValue);
                ta.refresh();
                break;
            case"ta1":
                rl.setTa1(newValue);
                ta.refresh();
                break; 
            case"ta2":
                rl.setTa2(newValue);
                ta.refresh();
                break;
            case"room":
              rl.setRoom(newValue);
              ta.refresh();
              break;
        }
    }

    @Override
    public void undoTransaction() {
        switch(propertype){
            case"section":
                rl.setSection(oldValue);
                ta.refresh();
                break;
            case"daytime":
                rl.setDay_Time(oldValue);
                ta.refresh();
                break;
            case"ta1":
                rl.setTa1(oldValue);
                ta.refresh();
                break; 
            case"ta2":
                rl.setTa2(oldValue);
                ta.refresh();
                break;
            case"room":
              rl.setRoom(oldValue);
              ta.refresh();
              break;
        }
        
        
        
        //le.setSectionProperty(oldValue);

       
    }  
}
