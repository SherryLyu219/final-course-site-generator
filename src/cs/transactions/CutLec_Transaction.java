/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import cs.data.LecturePropertype;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class CutLec_Transaction implements jTPS_Transaction{
    LecturePropertype leToCut;
     CourseSiteData data;
     
    public CutLec_Transaction(CourseSiteData initData, LecturePropertype initleToCut) {
        data = initData;
        leToCut = initleToCut;
    }
     @Override
    public void doTransaction() {
        data.removeFromLecture(leToCut);
        
//        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }

    @Override
    public void undoTransaction() {
        data.addToLecture(leToCut);
     //   app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }   
    
}
