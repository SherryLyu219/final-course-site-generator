/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import cs.data.RecLabPropertype;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class AddReclab_Transaction implements jTPS_Transaction{
    CourseSiteData data;
   // RecLabPropertype oldlr;
    RecLabPropertype newlr;
    TableView ta;
    String property;
    
    public AddReclab_Transaction(TableView initTa,CourseSiteData initData,RecLabPropertype initnewLec, String initpro) {
        data = initData;
        //oldlr = initoldLec;
        newlr = initnewLec;
        ta = initTa;
        property = initpro;
    }

    @Override
    public void doTransaction() {
        //data.removeFromLecture(oldLec);
        switch(property){
            case"lab":
                data.addToLab(newlr);
                ta.refresh();
                break;
            case"rec":
                data.addToRec(newlr);
                ta.refresh();
                break;
        }
    }

    @Override
    public void undoTransaction() {
        switch(property){
            case"lab":
                data.removeFromLab(newlr);
                ta.refresh();
                break;
            case"rec":
                data.removeFromRec(newlr);
                ta.refresh();
                break;
        }
      
    } 
    
}
