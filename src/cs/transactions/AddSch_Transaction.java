/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import cs.data.ScheduleItem;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class AddSch_Transaction implements jTPS_Transaction  {
    CourseSiteData data;
    ScheduleItem sch;
     public AddSch_Transaction(CourseSiteData initData, ScheduleItem initSch) {
        data = initData;
        sch = initSch;
    }
     @Override
    public void doTransaction() {
        data.addToSch(sch);        
    }

    @Override
    public void undoTransaction() {
        data.removeFromSch(sch);
    }
}
