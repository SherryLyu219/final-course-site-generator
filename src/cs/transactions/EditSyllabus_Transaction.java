/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class EditSyllabus_Transaction implements jTPS_Transaction  {
    CourseSiteData data;
    String propertype;
    String oldValue;
    String newValue;
    
    public EditSyllabus_Transaction(CourseSiteData initData, String initPropertype, String initOld, String initNew) {
        data = initData;
        propertype = initPropertype;
        oldValue = initOld;
        newValue = initNew;
    }

    @Override
    public void doTransaction() {
    
        data.addToSyllubus(newValue, propertype);
             
    }

    @Override
    public void undoTransaction() {
        data.addToSyllubus(oldValue, propertype);
       
    } 
}
