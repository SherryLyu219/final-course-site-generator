/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import cs.data.ScheduleItem;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class CutSch_Transaction implements jTPS_Transaction{
     ScheduleItem schToCut;
     CourseSiteData data;
     
    public CutSch_Transaction(CourseSiteData initData, ScheduleItem initschToCut) {
        data = initData;
        schToCut = initschToCut;
    }
     @Override
    public void doTransaction() {
        data.removeFromSch(schToCut);
        
//        app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }

    @Override
    public void undoTransaction() {
        data.addToSch(schToCut);
     //   app.getFoolproofModule().updateControls(APP_CLIPBOARD_FOOLPROOF_SETTINGS);
    }   
}
