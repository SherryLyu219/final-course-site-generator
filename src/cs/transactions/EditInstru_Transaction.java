/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class EditInstru_Transaction implements jTPS_Transaction {
    CourseSiteData data;
    String propertype;
    String oldValue;
    String newValue;
    
    public EditInstru_Transaction(CourseSiteData initData, String initPropertype, String initOld, String initNew) {
        data = initData;
        propertype = initPropertype;
        oldValue = initOld;
        newValue = initNew;
    }

    @Override
    public void doTransaction() {
        data.addInstructor(propertype, newValue);
             
    }

    @Override
    public void undoTransaction() {
        data.addInstructor(propertype, oldValue);
       
    } 
    
}
