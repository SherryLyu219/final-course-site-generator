/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.CourseSiteData;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class EditDate_Transaction implements jTPS_Transaction{
    CourseSiteData data;
    String type;
    String oldValue;
    String newValue;
    
    public EditDate_Transaction(CourseSiteData initData,String initType, String initOld, String initNew) {
        data = initData;
        type = initType;
        oldValue = initOld;
        newValue = initNew;
    }

    @Override
    public void doTransaction() {
    
        data.addToDatePicker(newValue, type);
             
    }

    @Override
    public void undoTransaction() {
        data.addToDatePicker(oldValue, type);
       
    } 
    
}
