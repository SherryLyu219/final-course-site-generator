/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import cs.data.ScheduleItem;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;

/**
 *
 * @author lvxinrui
 */
public class EditSchItem_Transaction implements jTPS_Transaction{
    TableView ta;
    ScheduleItem schToEdit;
    String oldType, newType;
    String oldDate, newDate;
    String oldTitle, newTitle;
    String oldTopic, newTopic;
    String oldLink, newLink;
    
    public EditSchItem_Transaction (TableView initta,ScheduleItem initSCHToEdit, 
             String type, String Date, String title,String topic,String link) {
        ta = initta;
        schToEdit = initSCHToEdit;
        oldType = initSCHToEdit.getType();
        oldDate = initSCHToEdit.getDate();
        oldTitle = initSCHToEdit.getTitle();
        oldTopic = initSCHToEdit.getTopic();
        oldLink = initSCHToEdit.getLink();
        newType = type;
        newDate = Date;
        newTitle = title;
        newTopic = topic;
        newLink = link;
    }


    @Override
    public void doTransaction() {
        schToEdit.setType(newType);
        schToEdit.setDate(newDate);
        schToEdit.setTitle(newTitle);
        schToEdit.setTopic(newTopic);
        schToEdit.setLink(newLink);
        ta.refresh();
    }

    @Override
    public void undoTransaction() {
        schToEdit.setType(oldType);
        schToEdit.setDate(oldDate);
        schToEdit.setTitle(oldTitle);
        schToEdit.setTopic(oldTopic);
        schToEdit.setLink(oldLink);
        ta.refresh();
    }
}
