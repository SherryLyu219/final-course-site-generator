/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.transactions;

import jtps.jTPS_Transaction;
import cs.data.CourseSiteData;
import cs.data.LecturePropertype;
import javafx.scene.control.TableView;

/**
 *
 * @author lvxinrui
 */
public class AddLec_Transaction implements jTPS_Transaction{
    CourseSiteData data;
    LecturePropertype oldLec;
    LecturePropertype newLec;
    TableView ta;
    
    public AddLec_Transaction(TableView initTa,CourseSiteData initData, LecturePropertype initoldLec, LecturePropertype initnewLec) {
        data = initData;
        oldLec = initoldLec;
        newLec = initnewLec;
        ta = initTa;
    }

    @Override
    public void doTransaction() {
        //data.removeFromLecture(oldLec);
        data.addToLecture(newLec); 
        ta.refresh();
    }

    @Override
    public void undoTransaction() {
        data.removeFromLecture(newLec);
       // data.addToLecture(oldLec);
        ta.refresh();
     
    } 
}
