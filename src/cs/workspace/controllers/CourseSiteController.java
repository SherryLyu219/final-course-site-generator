package cs.workspace.controllers;

import djf.modules.AppGUIModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import cs.CourseSiteGeneratorApp;
import static cs.CourseSitePropertyType.*;
import cs.data.CourseSiteData;
import cs.data.LecturePropertype;
import cs.data.RecLabPropertype;
import cs.data.ScheduleItem;
import cs.data.TAType;
import cs.data.TeachingAssistantPrototype;
import cs.data.TimeSlot;
import cs.data.TimeSlot.DayOfWeek;
import cs.transactions.AddLec_Transaction;
import cs.transactions.AddReclab_Transaction;
import cs.transactions.AddSch_Transaction;
import cs.transactions.AddTA_Transaction;
import cs.transactions.ChangeImage_Transaction;
import cs.transactions.CutLec_Transaction;
import cs.transactions.CutRecLab_Transaction;
import cs.transactions.CutSch_Transaction;
import cs.transactions.CutTA_Transaction;
import cs.transactions.EditCheckBox_Transaction;
import cs.transactions.EditCombo_Transaction;
import cs.transactions.EditDate_Transaction;
import cs.transactions.EditInstru_Transaction;
import cs.transactions.EditSchItem_Transaction;
import cs.transactions.EditSyllabus_Transaction;
import cs.transactions.EditTA_Transaction;
import cs.transactions.ToggleOfficeHours_Transaction;
import cs.workspace.dialogs.TADialog;
import static cs.workspace.style.CSStyle.CLASS_OH_TEXT_FIELD_ERROR;
import static djf.modules.AppGUIModule.ENABLED;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Scanner;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Callback;

/**
 *
 * @author McKillaGorilla
 */
public class CourseSiteController {

    CourseSiteGeneratorApp app;
    LecturePropertype oldLec;
    LecturePropertype newLec;
    RecLabPropertype oldRec;
    RecLabPropertype newRec;
    public CourseSiteController(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    public void processAddTA() {
        AppGUIModule gui = app.getGUIModule();
        TextField nameTF = (TextField) gui.getGUINode(OH_NAME_TEXT_FIELD);
        String name = nameTF.getText();
        TextField emailTF = (TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD);
        String email = emailTF.getText();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        TAType type = data.getSelectedType();
        if (data.isLegalNewTA(name, email)) {
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name.trim(), email.trim(), type);
            AddTA_Transaction addTATransaction = new AddTA_Transaction(data, ta);
            app.processTransaction(addTATransaction);

            // NOW CLEAR THE TEXT FIELDS
            nameTF.setText("");
            emailTF.setText("");
            nameTF.requestFocus();
        }
        app.getFoolproofModule().updateControls(OH_FOOLPROOF_SETTINGS);
    }

    public void processVerifyTA() {

    }

    public void processToggleOfficeHours() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        if (selectedCells.size() > 0) {
            TablePosition cell = selectedCells.get(0);
            int cellColumnNumber = cell.getColumn();
            CourseSiteData data = (CourseSiteData) app.getDataComponent();
            if (data.isDayOfWeekColumn(cellColumnNumber)) {
                DayOfWeek dow = data.getColumnDayOfWeek(cellColumnNumber);
                TableView<TeachingAssistantPrototype> taTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
                TeachingAssistantPrototype ta = taTableView.getSelectionModel().getSelectedItem();
                if (ta != null) {
                    TimeSlot timeSlot = officeHoursTableView.getSelectionModel().getSelectedItem();
                    ToggleOfficeHours_Transaction transaction = new ToggleOfficeHours_Transaction(data, timeSlot, dow, ta);
                    app.processTransaction(transaction);
                } else {
                    Stage window = app.getGUIModule().getWindow();
                    AppDialogsFacade.showMessageDialog(window, OH_NO_TA_SELECTED_TITLE, OH_NO_TA_SELECTED_CONTENT);
                }
            }
            int row = cell.getRow();
            cell.getTableView().refresh();
        }
    }

    public void processTypeTA() {
        app.getFoolproofModule().updateControls(OH_FOOLPROOF_SETTINGS);
    }

    public void processEditTA() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        if (data.isTASelected()) {
            TeachingAssistantPrototype taToEdit = data.getSelectedTA();
            TADialog taDialog = (TADialog) app.getGUIModule().getDialog(OH_TA_EDIT_DIALOG);
            taDialog.showEditDialog(taToEdit);
            TeachingAssistantPrototype editTA = taDialog.getEditTA();
            if (editTA != null) {
                EditTA_Transaction transaction = new EditTA_Transaction(taToEdit, editTA.getName(), editTA.getEmail(), editTA.getType());
                app.processTransaction(transaction);
            }
        }
    }
    public void processRemSch(){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        AppGUIModule gui = app.getGUIModule();
        TableView schTable = ((TableView)gui.getGUINode(CS_SCH_TABLE_VIEW));
        ScheduleItem schToCut = (ScheduleItem) schTable.getSelectionModel().getSelectedItem();
        if(schToCut != null){
        CutSch_Transaction transaction = new CutSch_Transaction(data, schToCut);
        app.processTransaction(transaction);
        }else{}
        
    }
    public void processRemLec(){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        AppGUIModule gui = app.getGUIModule();
        TableView<LecturePropertype> leTable = (TableView)gui.getGUINode(CS_MT_LEC_TABLE_VIEW);
        LecturePropertype leToCut = leTable.getSelectionModel().getSelectedItem();
        if(leToCut != null){
        CutLec_Transaction transaction = new CutLec_Transaction(data, leToCut);
        app.processTransaction(transaction);
        }else{}
    }
    public void processRemRecLab(String type){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        AppGUIModule gui = app.getGUIModule();
        switch(type){
            case"rec":
                TableView<RecLabPropertype> recTable = (TableView)gui.getGUINode(CS_MT_REC_TABLE_VIEW);
                RecLabPropertype recToCut = recTable.getSelectionModel().getSelectedItem();
                 if(recToCut != null){
                CutRecLab_Transaction transaction = new CutRecLab_Transaction(data, recToCut,"rec");
                app.processTransaction(transaction);
            }else{}
            case"lab":
                TableView<RecLabPropertype> labTable = (TableView)gui.getGUINode(CS_MT_LAB_TABLE_VIEW);
                RecLabPropertype labToCut = labTable.getSelectionModel().getSelectedItem(); 
                if(labToCut != null){
                CutRecLab_Transaction transaction = new CutRecLab_Transaction(data, labToCut,"lab");
                app.processTransaction(transaction);
            }else{}
        }
       
    }
    public void processCutTA() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        if (data.isTASelected()) {
            TeachingAssistantPrototype taToCut = data.getSelectedTA();
            HashMap<TimeSlot, ArrayList<DayOfWeek>> officeHours = data.getTATimeSlots(taToCut);
            CutTA_Transaction transaction = new CutTA_Transaction(app, taToCut, officeHours);
            app.processTransaction(transaction);
        }
    }

    public void processCheckTime() {
        AppGUIModule gui = app.getGUIModule();
        ComboBox officeHourStart = ((ComboBox) gui.getGUINode(OH_START_COMBO));
        ComboBox officeHourEnd = ((ComboBox) gui.getGUINode(OH_END_COMBO));
        if (officeHourStart.getSelectionModel().getSelectedItem() == null) {
            officeHourStart.getStyleClass().add(CLASS_OH_TEXT_FIELD_ERROR);
            officeHourStart.setStyle("-fx-border-color: red");
        }
        if (officeHourEnd.getSelectionModel().getSelectedItem() == null) {

            // officeHourEnd.getStyleClass().add(CLASS_OH_TEXT_FIELD_ERROR);
            officeHourEnd.setStyle("-fx-border-color: red");
        }
        if (officeHourStart.getSelectionModel().getSelectedItem() != null && officeHourEnd.getSelectionModel().getSelectedItem() != null) {
            String startTime = officeHourStart.getSelectionModel().getSelectedItem().toString();
            String endTime = officeHourEnd.getSelectionModel().getSelectedItem().toString();

            String startTimeint = startTime.replace(":", "");
            startTimeint = startTimeint.replace(startTimeint.substring(startTimeint.length() - 2), "");
            String endTimeint = endTime.replace(":", "");
            endTimeint = endTimeint.replace(endTimeint.substring(endTimeint.length() - 2), "");
            int Start = Integer.parseInt(startTimeint);
            int End = Integer.parseInt(endTimeint);
            if (startTime.contains("pm")) {
                if (!(startTime.equals("12:00pm")) && !(startTime.equals("12:30pm"))) {
                    Start = Start + 1200;
                }
            }
            if (endTime.contains("pm")) {
                if (!(endTime.equals("12:00pm")) && !(endTime.equals("12:30pm"))) {
                    End = End + 1200;
                }
            }
            //System.out.println(startTime);
            //System.out.print(endTime);

            if (End < Start) {
                officeHourEnd.setStyle("-fx-border-color: red");
            } else {
                officeHourStart.setStyle("-fx-border-color: black");
                officeHourEnd.setStyle("-fx-border-color: black");
                processSelectTimeRange(startTime, endTime);
            }
        }
    }
    public void processAddToInstru(String pro, String oldValue, String newValue){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        EditInstru_Transaction Transaction = new EditInstru_Transaction(data,pro,oldValue, newValue);
        app.processTransaction(Transaction);
    }
    public void processAddtitle(String pro,String oldValue, String newValue){
         CourseSiteData data = (CourseSiteData) app.getDataComponent();
          AppGUIModule gui = app.getGUIModule();
          TextField TitleTF = (TextField) gui.getGUINode(CS_SITE_TITLE_TF);
          String text = TitleTF.getText();
          EditCombo_Transaction EditCombosTransaction = new EditCombo_Transaction(data,pro,oldValue, newValue);
          app.processTransaction(EditCombosTransaction);
          
    }
    public void processAddTocombo() throws IOException {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        HashMap allSitedata = data.getAllSiteData();
        AppGUIModule gui = app.getGUIModule();
        ComboBox Subject_combo = ((ComboBox) gui.getGUINode(CS_SITE_SUBJECT_COMBOBOX));
        ComboBox Number_combo = ((ComboBox) gui.getGUINode(CS_SITE_NUMBER_COMBOBOX));
        String subject = Subject_combo.getEditor().getText().trim();
        String number = Number_combo.getEditor().getText().trim();
        
        String filepath = "./work/subject.txt";
        File file = new File(filepath);
            
            Scanner sc = new Scanner(file);
            boolean x = false;
        while (sc.hasNextLine()){
            String a = sc.nextLine();
             if(a.equals(subject)){
                 x = true;
                 break;
             }          
        }
        if (x) {} else if (subject.equals("") || subject == null) {
        } else {
            FileWriter fw = new FileWriter(filepath, true);
            fw.write("\n" + subject);
            fw.close();
            ((ArrayList) allSitedata.get("subject")).add(subject);
            Subject_combo.getItems().add(subject);
            Subject_combo.getSelectionModel().select(subject);
        }
        
        String filepathnum = "./work/number.txt";
       File filenum = new File(filepathnum);
            
            Scanner scnum = new Scanner(filenum);
            boolean y = false;
        while (scnum.hasNextLine()){
            String a = scnum.nextLine();
             if(a.equals(number)){
                 y = true;
                 break;
             }          
        }

        if (y) {
        }else if (number.equals("") || number == null) {
        }  else {
            FileWriter fwnum = new FileWriter(filepathnum, true);
            fwnum.write("\n" + number);
            fwnum.close();
            ((ArrayList) allSitedata.get("number")).add(number);
            Number_combo.getItems().add(number);
            Number_combo.getSelectionModel().select(number);
        }
        //processChangeSiteCombo();

    }
    public void processChangeImage(String oldpath, String newpath,String type){
        AppGUIModule gui = app.getGUIModule(); 
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        switch(type){
            case"fav":
                ImageView iv1 = ((ImageView)gui.getGUINode(SITE_FAV_ICON));
                ChangeImage_Transaction Transaction = new ChangeImage_Transaction(data,iv1,oldpath, newpath,type);
                app.processTransaction(Transaction); 
                break;
            case"nav":
                ImageView iv2 = ((ImageView)gui.getGUINode(SITE_STYLE_ICON));
                ChangeImage_Transaction Transaction_iv2 = new ChangeImage_Transaction(data,iv2,oldpath, newpath,type);
                app.processTransaction(Transaction_iv2); 
                break;
            case"left":
                ImageView iv3 = ((ImageView)gui.getGUINode(SITE_STYLE_LEFT_ICON));
                ChangeImage_Transaction Transaction_iv3 = new ChangeImage_Transaction(data,iv3,oldpath, newpath,type);
                app.processTransaction(Transaction_iv3); 
                break;
            case"right":
                ImageView iv4 = ((ImageView)gui.getGUINode(SITE_STYLE_RIGHT_ICON));
                ChangeImage_Transaction Transaction_iv4 = new ChangeImage_Transaction(data,iv4,oldpath, newpath,type);
                app.processTransaction(Transaction_iv4); 
                break;
        }
    }
    public void processChangeCombo(String property, String oldValue, String newValue){
          CourseSiteData data = (CourseSiteData) app.getDataComponent();
          EditCombo_Transaction EditCombosTransaction = new EditCombo_Transaction(data,property,oldValue, newValue);
          app.processTransaction(EditCombosTransaction);
          this.processChangeSiteCombo();
    }
    public void processCheckStartEnd(){
        AppGUIModule gui = app.getGUIModule();
        DatePicker datePicker1 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER));
        DatePicker datePicker2 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER2)); 
         DatePicker datePicker3 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER3));
        LocalDate start = datePicker1.getValue();
        LocalDate end = datePicker2.getValue();
        
        if(start == null || end == null){
            if(start == null){
                datePicker1.setStyle("-fx-border-color: red");
            }
            if(end == null){
                datePicker2.setStyle("-fx-border-color: red");
            }
            datePicker3.setDisable(ENABLED);
            
        }else if(end.isBefore(start)){
            datePicker3.setDisable(ENABLED);
        }else{
            datePicker1.setStyle("-fx-border-color: transparent");
            datePicker2.setStyle("-fx-border-color: transparent");
            datePicker3.setDisable(false);
            processTimeFilter(start, end);
        }
    }
    
    public void processcheckSchduleItem(){
//        CourseSiteData data = (CourseSiteData) app.getDataComponent();
//        AppGUIModule gui = app.getGUIModule();
//        ComboBox option = ((ComboBox)gui.getGUINode(CS_SCH_TYPE_COMBO));
//        String type = (String)option.getSelectionModel().getSelectedItem(); 
//        Button addorupdate = ((Button)gui.getGUINode(CS_SCH_ADD_BUTTON));
//        DatePicker datePicker1 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER));
//        DatePicker datePicker2 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER2)); 
//        DatePicker datePicker3 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER3));
//        LocalDate date = datePicker3.getValue();
//        LocalDate start = datePicker1.getValue();
//        LocalDate end = datePicker2.getValue();
//        String dateStr = date.toString();
//        
//       
//        TextField title = ((TextField)gui.getGUINode(CS_SCH_TITLE_TEXTFIELD));
//        String titleString = title.getText().trim();
//        TextField topic = ((TextField)gui.getGUINode(CS_SCH_TOPIC_TEXTFIELD));
//        String topicString = topic.getText().trim();
//        if(type == null||dateStr == null||titleString.equals("")||topicString.equals("")||date.isBefore(start)|| date.isAfter(end)){
//            if(date.isBefore(start)){
//                datePicker3.setStyle("-fx-border-color: red");
//            }else{
//                datePicker3.setStyle("-fx-border-color: transparent");
//            }
//            if(date.isAfter(end)){
//                datePicker3.setStyle("-fx-border-color: red");
//            }else{
//                datePicker3.setStyle("-fx-border-color: transparent");
//            }
//            addorupdate.setDisable(ENABLED);
//        }else{
//            addorupdate.setDisable(false);
//            datePicker3.setStyle("-fx-border-color: transparent");
//        }
    }
    public void processUpdateScheduleItem(){
        AppGUIModule gui = app.getGUIModule();
        TableView schTable = ((TableView)gui.getGUINode(CS_SCH_TABLE_VIEW));
        ScheduleItem sch = (ScheduleItem)schTable.getSelectionModel().getSelectedItem();
        ComboBox option = ((ComboBox)gui.getGUINode(CS_SCH_TYPE_COMBO));
        String type = (String)option.getSelectionModel().getSelectedItem();
        DatePicker datePicker3 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER3));
       
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yy"); 
        String dateStr = datePicker3.getValue().format(formatter);
       // String dateStr = date.toString();
        System.out.print(dateStr);
        
        TextField title = ((TextField)gui.getGUINode(CS_SCH_TITLE_TEXTFIELD));
        String titleString = title.getText().trim();
        TextField topic = ((TextField)gui.getGUINode(CS_SCH_TOPIC_TEXTFIELD));
        String topicString = topic.getText().trim();
        TextField link = ((TextField)gui.getGUINode(CS_SCH_LINK_TEXTFIELD));
        String linkStr = link.getText().trim();
        EditSchItem_Transaction Transaction = new EditSchItem_Transaction(schTable,sch,type,dateStr,titleString,topicString,linkStr);
        app.processTransaction(Transaction);  
        
    }
    public void processSchduleItem(){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        AppGUIModule gui = app.getGUIModule();
        ComboBox option = ((ComboBox)gui.getGUINode(CS_SCH_TYPE_COMBO));
        String type = (String)option.getSelectionModel().getSelectedItem();
        DatePicker datePicker3 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER3));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yy",Locale.US); 
        String dateStr;
        if(datePicker3.getValue() == null){
            dateStr = "";
       }else{
           dateStr = datePicker3.getValue().format(formatter);
        }
    
        TextField link = ((TextField)gui.getGUINode(CS_SCH_LINK_TEXTFIELD));
        String linkStr = link.getText().trim();
        TextField title = ((TextField)gui.getGUINode(CS_SCH_TITLE_TEXTFIELD));
        String titleString = title.getText().trim();
        TextField topic = ((TextField)gui.getGUINode(CS_SCH_TOPIC_TEXTFIELD));
        String topicString = topic.getText().trim();
        ScheduleItem sch = new ScheduleItem(type, dateStr, titleString, topicString,linkStr);
        AddSch_Transaction Transaction = new AddSch_Transaction(data,sch);
        app.processTransaction(Transaction);  
        
    }
    public void processTransDown(){
        AppGUIModule gui = app.getGUIModule();
        TableView schTable = ((TableView)gui.getGUINode(CS_SCH_TABLE_VIEW));
        ScheduleItem sch = (ScheduleItem)schTable.getSelectionModel().getSelectedItem();
        ComboBox option = ((ComboBox)gui.getGUINode(CS_SCH_TYPE_COMBO));
        DatePicker datePicker3 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER3));
        TextField title = ((TextField)gui.getGUINode(CS_SCH_TITLE_TEXTFIELD));
        TextField topic = ((TextField)gui.getGUINode(CS_SCH_TOPIC_TEXTFIELD));
        if(sch != null){
        String optionStr = sch.getType();
        String dateStr = sch.getDate();
        String titleStr = sch.getTitle();
        String topicStr = sch.getTopic();
        
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yy");
        LocalDate localDate = LocalDate.parse(dateStr, formatter);
        option.getSelectionModel().select(optionStr);
        datePicker3.setValue(localDate);
        title.setText(titleStr);
        topic.setText(topicStr);
        }
        
    }
    public void processChangeSyllabus(String property,String oldValue,String newValue){
         CourseSiteData data = (CourseSiteData) app.getDataComponent();
          EditSyllabus_Transaction EditSyllabusTransaction = new EditSyllabus_Transaction(data,property,oldValue, newValue);
          app.processTransaction(EditSyllabusTransaction);  
    }
    public void processEditCheckBox(String property, Boolean oldValue, Boolean newValue){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        EditCheckBox_Transaction  transaction = new EditCheckBox_Transaction(data, property, oldValue, newValue);
        app.processTransaction(transaction);
    }
    public void processCheckCheckBox(){
        AppGUIModule gui = app.getGUIModule();
        CheckBox homeBox = ((CheckBox)gui.getGUINode(CS_SITE_HOME_CHECKBOX));
        CheckBox SyllabusBox = ((CheckBox)gui.getGUINode(CS_SITE_SYLLABUS_CHECKBOX));
        CheckBox ScheduleBox = ((CheckBox)gui.getGUINode(CS_SITE_SCHEDULE_CHECKBOX));
        CheckBox HwsBox = ((CheckBox)gui.getGUINode(CS_SITE_HWS_CHECKBOX));
        

        if(homeBox.isSelected()||SyllabusBox.isSelected()||ScheduleBox.isSelected()||HwsBox.isSelected()){
            homeBox.setStyle("-fx-border-color: transparent");
            SyllabusBox.setStyle("-fx-border-color: transparent");
            ScheduleBox.setStyle("-fx-border-color: transparent");
            HwsBox.setStyle("-fx-border-color: transparent");
        }else{
            homeBox.setStyle("-fx-border-color: red");
            SyllabusBox.setStyle("-fx-border-color: red");
            ScheduleBox.setStyle("-fx-border-color: red");
            HwsBox.setStyle("-fx-border-color: red");
        }
    }
    public void processChangeDate(String property, String oldValue,String newValue){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        EditDate_Transaction  transaction = new EditDate_Transaction(data, property, oldValue, newValue);
        app.processTransaction(transaction);
    }
    public void processAddinitLec(){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        AppGUIModule gui = app.getGUIModule();
        TableView lecTable =(TableView)gui.getGUINode(CS_MT_LEC_TABLE_VIEW);
        lecTable.setEditable(ENABLED);
        oldLec = (LecturePropertype)lecTable.getSelectionModel().getSelectedItem();
        newLec = new LecturePropertype("?","?", "?","?");
//        newLec.getSectionStringPro().addListener(e->{
//            System.out.println("changed");
//        });
        AddLec_Transaction addLECTransaction = new AddLec_Transaction(lecTable,data,oldLec, newLec);
        app.processTransaction(addLECTransaction);  
        oldLec = (LecturePropertype)lecTable.getSelectionModel().getSelectedItem();
        //TableView<LecturePropertype> LectureTableView = (TableView) gui.getGUINode(CS_MT_LEC_TABLE_VIEW);
        //LectureTableView.setEditable(true);
        //ObservableList<TablePosition> selectedCells = officeHoursTableView.getSelectionModel().getSelectedCells();
        
    }
    public void processAddinitRecLab(String propertype){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        AppGUIModule gui = app.getGUIModule();
        switch(propertype){
            case"rec":
                TableView recTable =(TableView)gui.getGUINode(CS_MT_REC_TABLE_VIEW);
                recTable.setEditable(ENABLED);
                oldRec = (RecLabPropertype)recTable.getSelectionModel().getSelectedItem();
                newRec = new RecLabPropertype("?","?","?","?","?");
                AddReclab_Transaction transaction = new AddReclab_Transaction(recTable, data, newRec, propertype);
                app.processTransaction(transaction); 
                break;
            case"lab":
                TableView labTable =(TableView)gui.getGUINode(CS_MT_LAB_TABLE_VIEW);
                labTable.setEditable(ENABLED); 
                oldRec = (RecLabPropertype)labTable.getSelectionModel().getSelectedItem();
                newRec = new RecLabPropertype("?","?","?","?","?");
                AddReclab_Transaction rec_transaction = new AddReclab_Transaction(labTable, data, newRec, propertype);
                app.processTransaction(rec_transaction); 
                break;
        }
    }
    public void setString(String type,String filePath){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        switch(type){
                case "Fav":
                    data.setStringFav(filePath);
                case "Nav":
                    data.setNav(filePath);
                case "Left":
                    data.setLeft(filePath);
                case "Right":
                    data.setRight(filePath);
        }
    }
    public void processChangeSiteCombo() {
        AppGUIModule gui = app.getGUIModule();
        ComboBox Subject_combo = ((ComboBox) gui.getGUINode(CS_SITE_SUBJECT_COMBOBOX));
        ComboBox Number_combo = ((ComboBox) gui.getGUINode(CS_SITE_NUMBER_COMBOBOX));
        //if(Subject_combo.getSelectionModel().getSelectedItem()!=null){
        //String Subject = Subject_combo.getSelectionModel().getSelectedItem().toString();
        //}
        if(Number_combo.getSelectionModel().getSelectedItem()!= null &&Subject_combo.getSelectionModel().getSelectedItem()!=null){
        String Number = Number_combo.getSelectionModel().getSelectedItem().toString();
        String Subject = Subject_combo.getSelectionModel().getSelectedItem().toString();
        ComboBox Semester_combo = ((ComboBox) gui.getGUINode(CS_SITE_SEMESTER_COMBOBOX));
        ComboBox Year_combo = ((ComboBox) gui.getGUINode(CS_SITE_YEAR_COMBOBOX));
        String Semester = Semester_combo.getSelectionModel().getSelectedItem().toString();
        String Year = Year_combo.getSelectionModel().getSelectedItem().toString();

        Text Export = ((Text) gui.getGUINode(CS_SITE_EXPOR_TEXT));
        Export.setText("./export/" + Subject + "_" + Number + "_" + Semester + "_" + Year + "/public_html");
        //Subject_combo.
        }
    }
    public void processTimeFilter(LocalDate Start, LocalDate End){
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ArrayList scheduleData = data.getSchData();
        ObservableList schedule = data.getSchOL();
        int start = 0;
        int end = 0;
        schedule.clear();
        for(int i = 0; i < scheduleData.size(); i++){
            ScheduleItem sch = (ScheduleItem) scheduleData.get(i);
            String date = sch.getDate();
            if(!date.equals("")){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yy"); 
                LocalDate dateTime = LocalDate.parse(date, formatter);
                if(dateTime.isAfter(Start)&& dateTime.isBefore(End)){
                    schedule.add(sch);  
                }
            }
            

    }
        
        
    }
    public void processSelectTimeRange(String StartTime, String EndTime) {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        ArrayList officeHoursData = data.getOfficeHourData();
        ObservableList officeHour = data.getObservableList();
        int start = 0;
        int end = 0;

        /*
        for(int i = 0; i< officeHoursData.size(); i++){
            officeHour.add(officeHoursData.get(i));
        }
         */
        officeHour.clear();
        for (int i = 0; i < officeHoursData.size(); i++) {
            String timeSlotStartTime = ((TimeSlot) officeHoursData.get(i)).getStartTime();
            //System.out.print(timeSlotStartTime);
            if (timeSlotStartTime.equals(StartTime)) {
                start = i;
            } else if (timeSlotStartTime.equals(EndTime)) {
                end = i;
            }
        }
        //System.out.println(start);
        //System.out.println(end);
        for (int i = start; i < end + 1; i++) {
            officeHour.add(officeHoursData.get(i));
        }

        //String[] startT = StartTime.split(":");
        //System.out.print("lalalala");
    }

    public void processChangeTimeRange() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();

    }

    public void processSelectAllTAs() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        data.selectTAs(TAType.All);
    }

    public void processSelectGradTAs() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        data.selectTAs(TAType.Graduate);
    }

    public void processSelectUndergradTAs() {
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        data.selectTAs(TAType.Undergraduate);
    }

    public void processSelectTA() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.refresh();
    }

}
