/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.workspace;

import static djf.AppPropertyType.PREF_WIDTH;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import cs.CourseSiteGeneratorApp;
import cs.CourseSitePropertyType;
import cs.data.TeachingAssistantPrototype;
import cs.data.TimeSlot;
import cs.workspace.controllers.CourseSiteController;
import cs.workspace.dialogs.TADialog;
import cs.workspace.foolproof.OfficeHoursFoolproofDesign;
import static cs.workspace.style.CSStyle.CLASS_BANNER_COMBO_PANE;
import static cs.workspace.style.CSStyle.CLASS_BANNER_LABEL_PANE;
import static cs.workspace.style.CSStyle.CLASS_BANNER_PANE;
import static cs.workspace.style.CSStyle.CLASS_CS_PANE;
import static cs.workspace.style.CSStyle.CLASS_OH_BOX;
import static cs.workspace.style.CSStyle.CLASS_OH_BUTTON;
import static cs.workspace.style.CSStyle.CLASS_OH_CENTERED_COLUMN;
import static cs.workspace.style.CSStyle.CLASS_OH_COLUMN;
import static cs.workspace.style.CSStyle.CLASS_OH_DAY_OF_WEEK_COLUMN;
import static cs.workspace.style.CSStyle.CLASS_OH_HEADER_LABEL;
import static cs.workspace.style.CSStyle.CLASS_OH_OFFICE_HOURS_TABLE_VIEW;
import static cs.workspace.style.CSStyle.CLASS_OH_PANE;
import static cs.workspace.style.CSStyle.CLASS_OH_RADIO_BOX;
import static cs.workspace.style.CSStyle.CLASS_OH_RADIO_BUTTON;
import static cs.workspace.style.CSStyle.CLASS_OH_TABLE_VIEW;
import static cs.workspace.style.CSStyle.CLASS_OH_TEXT_FIELD;
import static cs.workspace.style.CSStyle.CLASS_OH_TIME_COLUMN;
import static cs.workspace.style.CSStyle.CLASS_PAGE_LABEL_PANE;
import static cs.workspace.style.CSStyle.CLASS_SITE_CHECKBOX_PANE;
import static cs.workspace.style.CSStyle.CLASS_SITE_PAGE_PANE;
import static cs.workspace.style.CSStyle.CLASS_SITE_PANE;
import static cs.workspace.style.CSStyle.CLASS_SITE_STYLE_BUTTON;
import static cs.workspace.style.CSStyle.CLASS_SITE_STYLE_PANE;
import static cs.workspace.style.CSStyle.CLASS_SITE_TEXTFIELD;
import static cs.workspace.style.CSStyle.CLASS_TAB;
import static cs.workspace.style.CSStyle.CLASS_TITLED_PANE;
import static cs.CourseSitePropertyType.*;
import cs.data.CourseSiteData;
import cs.data.LecturePropertype;
import cs.data.RecLabPropertype;
import cs.transactions.AddLec_Transaction;
import cs.transactions.MeetingTimeRL_Transaction;
import cs.transactions.MeetingTime_Transaction;
import static cs.workspace.style.CSStyle.CLASS_MT_BUTTON;
import static cs.workspace.style.CSStyle.CLASS_TEXTAREA;
import static djf.AppPropertyType.LOAD_WORK_TITLE;
import djf.ui.dialogs.AppDialogsFacade;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;
import properties_manager.PropertiesManager;
/**
 *
 * @author lvxinrui
 */
public class CourseSiteWorkspace extends AppWorkspaceComponent{
    String oldTextValue = "";
    String newTextValue = "";
    String newSection;
    String oldSection;
    LecturePropertype newLec;
    LecturePropertype oldLec;
    String oldFilePath;
    String newFilePath;
    String oldnavPath;
    String newnavPath;
    String oldleftPath;
    String newleftPath;
    String oldrightPath;
    String newrightPath;
    Boolean newState;
    Boolean oldState;
    Boolean newsyState;
    Boolean oldsyState;
     public CourseSiteWorkspace(CourseSiteGeneratorApp app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        initControllers();

        // 
        initFoolproofDesign();

        // INIT DIALOGS
        initDialogs();
    }
     private void initDialogs() {
        TADialog taDialog = new TADialog((CourseSiteGeneratorApp) app);
        app.getGUIModule().addDialog(OH_TA_EDIT_DIALOG, taDialog);
    }
     private void initLayout(){
          PropertiesManager props = PropertiesManager.getPropertiesManager();
        CourseSiteData data = (CourseSiteData) app.getDataComponent();
        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder csBuilder = app.getGUIModule().getNodesBuilder();
      //################################################################################################ 
        //Creat site tab pane
        TabPane layout = csBuilder.bulidTabPane(CS_TAB_PANE, null,CLASS_CS_PANE,ENABLED);
        Tab tabSite = csBuilder.buildTab(CS_TAB_SITE, CLASS_TAB);
  
        ScrollPane siteScrollPane = new ScrollPane();
        
        VBox sitePane = csBuilder.buildVBox(CS_SITE_PANE, null, CLASS_SITE_PANE ,ENABLED);
        
        //Creat bannerPane
        GridPane bannerPane = csBuilder.buildGridPane(CS_SITE_BANNER, sitePane, CLASS_BANNER_PANE, ENABLED);
        bannerPane.setHgap(20);
        bannerPane.setVgap(7);
        Label Banner = csBuilder.buildLabel(CS_SITE_BANNER_LABEL, bannerPane,0,0,1,1, CLASS_BANNER_LABEL_PANE, ENABLED);
        //Creat subject
        Label Subject = csBuilder.buildLabel(CS_SITE_SUBJECT_LABEL, bannerPane, 0, 1, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
      
        ComboBox Subject_combo = csBuilder.buildComboBox(CS_SITE_SUBJECT_COMBOBOX, bannerPane, 1, 1, 1, 1, CLASS_BANNER_COMBO_PANE, ENABLED,"CSE", "CSE");
       // Subject_combo.getItems().addAll("CSE","AMS","ISE");
        Subject_combo.setEditable(true); 
        //Subject_combo.getSelectionModel().selectFirst();
        //Creat number
        Label Number = csBuilder.buildLabel(CS_SITE_NUMBER_LABEL, bannerPane,2,1,1,1,CLASS_BANNER_LABEL_PANE, ENABLED);
        ComboBox Number_combo = csBuilder.buildComboBox(CS_SITE_NUMBER_COMBOBOX, bannerPane, 3, 1, 1, 1, CLASS_BANNER_COMBO_PANE, ENABLED,OH_OK_PROMPT, CLASS_BANNER_PANE);
        //Number_combo.getItems().addAll("219","220","131","346");
        Number_combo.setEditable(true);
        //Number_combo.getSelectionModel().selectFirst();
        //Creat semester
        Label Semester = csBuilder.buildLabel(CS_SITE_SEMESTER_LABEL, bannerPane,0,2,1,1,CLASS_BANNER_LABEL_PANE, ENABLED);
        ComboBox Semester_combo = csBuilder.buildComboBox(CS_SITE_SEMESTER_COMBOBOX, bannerPane, 1, 2, 1, 1, CLASS_BANNER_COMBO_PANE, ENABLED, OH_OK_PROMPT, CLASS_BANNER_PANE);
        Semester_combo.getItems().addAll("Fall","Spring","Summer Session1","Summer Session2","Winter Session1","Winter Session2");
        Semester_combo.setEditable(true);
        Semester_combo.getSelectionModel().selectFirst();
        //Creat year
        Label Year = csBuilder.buildLabel(CS_SITE_YEAR_LABEL, bannerPane,2,2,1,1,CLASS_BANNER_LABEL_PANE, ENABLED);
        ComboBox Year_combo = csBuilder.buildComboBox(CS_SITE_YEAR_COMBOBOX, bannerPane, 3, 2, 1, 1, CLASS_BANNER_COMBO_PANE, ENABLED,OH_OK_PROMPT, CLASS_BANNER_PANE);
        Year_combo.getItems().addAll("2018","2019","2020","2017");
        Year_combo.setEditable(true);
        Year_combo.getSelectionModel().selectFirst();
        
        Label Title = csBuilder.buildLabel(CS_SITE_TITLE_LABEL, bannerPane, 0, 3, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
        TextField titleTextField = csBuilder.buildTextField(CS_SITE_TITLE_TF, bannerPane, 1, 3, 1, 1, CLASS_SITE_TEXTFIELD, ENABLED);
        
        Label Expordir = csBuilder.buildLabel(CS_SITE_EXPOR_LABEL, bannerPane, 0, 4, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
        Text exporDir = csBuilder.buildText(CS_SITE_EXPOR_TEXT, bannerPane, 1, 4, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
     
        exporDir.setText("./export/CSE_219_Fall_2018/public_html");
       
        //Create pages pane
        HBox pagePane = csBuilder.buildHBox(CS_SITE_PAGE_PANE, sitePane, CLASS_SITE_PAGE_PANE, ENABLED);
        pagePane.setSpacing(20);
        Label Pages = csBuilder.buildLabel(CS_SITE_PAGE_LABEL, pagePane, CLASS_PAGE_LABEL_PANE, ENABLED);
        
        CheckBox Home = csBuilder.buildCheckBox(CS_SITE_HOME_CHECKBOX, pagePane, CLASS_SITE_CHECKBOX_PANE, ENABLED);
        Home.setSelected(true);
        CheckBox Syllabus = csBuilder.buildCheckBox(CS_SITE_SYLLABUS_CHECKBOX, pagePane, CLASS_SITE_CHECKBOX_PANE, ENABLED);
        CheckBox Schedule = csBuilder.buildCheckBox(CS_SITE_SCHEDULE_CHECKBOX, pagePane, CLASS_SITE_CHECKBOX_PANE, ENABLED);
        CheckBox HWs = csBuilder.buildCheckBox(CS_SITE_HWS_CHECKBOX, pagePane, CLASS_SITE_CHECKBOX_PANE, ENABLED);
        
        //Create Style pane
        VBox StylePane = csBuilder.buildVBox(CS_SITE_STYLE_PANE, sitePane, CLASS_SITE_STYLE_PANE, ENABLED);
        StylePane.setSpacing(5);
        Label Style = csBuilder.buildLabel(CS_SITE_STYLE_LABEL, StylePane, CLASS_PAGE_LABEL_PANE, ENABLED);
        HBox FavBox = csBuilder.buildHBox(CS_SITE_STYLE_FAV_HBOX, StylePane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Button FaviconButton = csBuilder.buildTextButton(SITE_STYLE_FAVCON_BUTTON, FavBox, CLASS_SITE_STYLE_BUTTON, ENABLED);
        ImageView iv1 = csBuilder.buildImageView(SITE_FAV_ICON, FavBox,CLASS_SITE_STYLE_BUTTON,ENABLED);
        Image image= new Image("file:./images/sblogo.png");
        oldFilePath ="./images/sblogo.png";
       // data.setStringFav("./images/sblogo.png");
        //ImageView iv1 = new ImageView(new Image("file:./images/sblogo.png"));
        iv1.setImage(image);
       // FavBox.getChildren().add(iv1);
        FavBox.setSpacing(40);
        
        HBox NavBox = csBuilder.buildHBox(CS_SITE_STYLE_NAV_HBOX, StylePane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Button NavbarButton = csBuilder.buildTextButton(SITE_STYLE_NAVBAR_BUTTON, NavBox, CLASS_SITE_STYLE_BUTTON, ENABLED);
       ImageView iv2 = csBuilder.buildImageView(SITE_STYLE_ICON, NavBox,CLASS_SITE_STYLE_BUTTON,ENABLED);
       Image image2 = new Image("file:./images/SBUDarkRed.png");
       oldnavPath = "./images/SBUDarkRed.png";
       iv2.setImage(image2);
        //NavBox.getChildren().add(iv2);
       // NavBox.setSpacing(40);
        HBox LeftBox = csBuilder.buildHBox(CS_SITE_STYLE_LEFT_HBOX, StylePane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Button LeftFooterButton = csBuilder.buildTextButton(SITE_STYLE_LEFTFOOT_BUTTON, LeftBox, CLASS_SITE_STYLE_BUTTON, ENABLED);
        ImageView iv3 = csBuilder.buildImageView(SITE_STYLE_LEFT_ICON, LeftBox,CLASS_SITE_STYLE_BUTTON,ENABLED);
        Image image3 = new Image("file:./images/SBUWhite.jpg");
        oldleftPath = "./images/SBUWhite.jpg";
        iv3.setImage(image3);
       // LeftBox.getChildren().add(iv3);
        //LeftBox.setSpacing(40);
        HBox RightBox = csBuilder.buildHBox(CS_SITE_STYLE_RIGH_HBOX, StylePane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Button RightFooterButton = csBuilder.buildTextButton(SITE_STYLE_RIGHTFOOT_BUTTON, RightBox, CLASS_SITE_STYLE_BUTTON, ENABLED);
        ImageView iv4 = csBuilder.buildImageView(SITE_STYLE_RIGHT_ICON, RightBox,CLASS_SITE_STYLE_BUTTON,ENABLED);
        Image image4 = new Image("file:./images/SBUCSLogo.png");
        oldrightPath = "./images/SBUCSLogo.png";
        iv4.setImage(image4);
        //RightBox.getChildren().add(iv4);
        //RightBox.setSpacing(40);
        HBox styleSheetBox =  csBuilder.buildHBox(CS_SITE_STYLE_SHEET_HBOX, StylePane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Label StyleSheetLabel = csBuilder.buildLabel(SITE_STYLE_STYLESHEET_LABEL, styleSheetBox, CLASS_BANNER_LABEL_PANE, ENABLED);
        ComboBox StyleSheet_combo = csBuilder.buildComboBox(SITE_STYLE_COMBO, OH_OK_PROMPT, LeftFooterButton, styleSheetBox, CLASS_BANNER_COMBO_PANE, ENABLED);
        //StyleSheet_combo.getItems().addAll("seawolf.css");
        StyleSheet_combo.setEditable(true);
        StyleSheet_combo.getSelectionModel().selectFirst();
        Label NoteLabel = csBuilder.buildLabel(SITE_STYLE_STYLE_NOTE_LABEL, StylePane, CLASS_BANNER_LABEL_PANE, ENABLED);

        StylePane.setSpacing(10);
        //Label NoteLabel = csBuilder.buildLabel(SITE_STYLE_NOTELABEL, StylePane, CLASS_BANNER_LABEL_PANE, ENABLED);
        
        //Create Instructor Pane
        VBox InstructorPane = csBuilder.buildVBox(CS_SITE_INSTRUCTOR_PANE, sitePane, CLASS_SITE_PAGE_PANE, ENABLED);
        Label InstructorLabel = csBuilder.buildLabel(CS_SITE_INSTRU_LABEL, InstructorPane,CLASS_PAGE_LABEL_PANE, ENABLED);
        HBox InstructorFirst = csBuilder.buildHBox(CS_SITE_INSTRU_1_HBOX, InstructorPane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Label NameLabel = csBuilder.buildLabel(CS_SITE_NAME_LABEL,InstructorFirst, CLASS_BANNER_LABEL_PANE,ENABLED);
        TextField NameTextField = csBuilder.buildTextField(CS_SITE_NAME_TEXTFIELD, InstructorFirst, CLASS_SITE_TEXTFIELD, ENABLED);
        Label RoomLabel = csBuilder.buildLabel(CS_SITE_ROOM_LABEL, InstructorFirst, CLASS_BANNER_LABEL_PANE, ENABLED);
        TextField RoomTextField = csBuilder.buildTextField(CS_SITE_ROOM_TEXTFIELD, InstructorFirst, CLASS_SITE_TEXTFIELD, ENABLED);
        HBox InstructorSecond = csBuilder.buildHBox(CS_SITE_INSTRU_2_HBOX, InstructorPane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Label EmailLabel = csBuilder.buildLabel(CS_SITE_EMAIL_LABEL, InstructorSecond, CLASS_BANNER_LABEL_PANE, ENABLED);
        TextField EmailTextField = csBuilder.buildTextField(CS_SITE_EMAIL_TEXTFIELD, InstructorSecond, CLASS_SITE_TEXTFIELD, ENABLED);
        Label HomePageLabel = csBuilder.buildLabel(CS_SITE_HOMEPAGE_LABEL,InstructorSecond, CLASS_BANNER_LABEL_PANE, ENABLED);
        TextField HomePageTextField = csBuilder.buildTextField(CS_SITE_HOMEPAGE_TEXTFIELD, InstructorSecond, CLASS_SITE_TEXTFIELD, ENABLED);
        
        TitledPane tp = csBuilder.buildTitlePane(CS_SITE_OFFICEHOUR_TITLED_PANE, InstructorPane, CLASS_TITLED_PANE,CS_SITE_OFFICE_TA_PANE,CLASS_TEXTAREA, ENABLED);
        tp.setExpanded(false);
        
        InstructorFirst.setSpacing(35);
        InstructorSecond.setSpacing(35);
        siteScrollPane.setContent(sitePane);
        tabSite.setContent(siteScrollPane);
        InstructorPane.setSpacing(10);
         //################################################################################################
       // Creat syllabus tab pane
        Tab tabSyllabus = csBuilder.buildTab(CS_TAB_SYLLABUS, CLASS_TAB);
        ScrollPane syllabusScrollPane = new ScrollPane();
       
        VBox syllabusPane = csBuilder.buildVBox(CS_SYLL_PANE, null, CLASS_SITE_PANE, ENABLED);
        TitledPane descriptionTitle = csBuilder.buildTitlePane(CS_SYLL_DES_TITLE, syllabusPane, CLASS_TITLED_PANE, CS_SYLL_DES_TA_PANE, CLASS_TEXTAREA, ENABLED);
        descriptionTitle.setExpanded(false);
       // TextArea descriptionText = csBuilder.buildTextArea(Home, descriptionTitle, CLASS_TAB, ENABLED);
        TitledPane topicTitle = csBuilder.buildTitlePane(CS_STYLL_TOPIC_TITLE, syllabusPane, CLASS_TITLED_PANE, CS_STYLL_TOPIC_TA,  CLASS_TEXTAREA, ENABLED);
        topicTitle.setExpanded(false);
        TitledPane prerequistesTitle = csBuilder.buildTitlePane(CS_STYLL_PREREQUIST_TITLE, syllabusPane, CLASS_TITLED_PANE,CS_STYLL_PREREQUIST_TA, CLASS_TEXTAREA, ENABLED);
        prerequistesTitle.setExpanded(false);
        TitledPane outcomesTitle = csBuilder.buildTitlePane(CS_STYLL_OUTCOME_TITLE, syllabusPane, CLASS_TITLED_PANE,CS_STYLL_OUTCOME_TA, CLASS_TEXTAREA, ENABLED);
        outcomesTitle.setExpanded(false);
        TitledPane textBooksTitle = csBuilder.buildTitlePane(CS_STYLL_TEXTBOOK_TITLE, syllabusPane,CLASS_TITLED_PANE, CS_STYLL_TEXTBOOK_TA,CLASS_TEXTAREA, ENABLED);
        textBooksTitle.setExpanded(false);
        TitledPane gradedCompTitle = csBuilder.buildTitlePane(CS_STYLL_GRADEDCOMP_TITLE, syllabusPane, CLASS_TITLED_PANE, CS_STYLL_GRADEDCOMP_TA, CLASS_TEXTAREA, ENABLED);
        gradedCompTitle.setExpanded(false);
        TitledPane gradingnoteTitle = csBuilder.buildTitlePane(CS_STYLL_GRADENOTE_TITLE, syllabusPane, CLASS_TITLED_PANE, CS_STYLL_GRADENOTE_TA, CLASS_TEXTAREA, ENABLED);
        gradingnoteTitle.setExpanded(false);
        TitledPane academicdisTitle = csBuilder.buildTitlePane(CS_STYLL_ACADEMIC_TITLE, syllabusPane, CLASS_TITLED_PANE, CS_STYLL_ACADEMIC_TITLE_TA, CLASS_TEXTAREA, ENABLED);
        academicdisTitle.setExpanded(false);
        TitledPane specialassTitle = csBuilder.buildTitlePane(CS_STYLL_SPEASS_TITLE, syllabusPane, CLASS_TITLED_PANE, CS_STYLL_SPEASS_TITLE_TA, CLASS_TEXTAREA, ENABLED);
        specialassTitle.setExpanded(false);
        
        syllabusScrollPane.setContent(syllabusPane);
        tabSyllabus.setContent(syllabusScrollPane);
         //################################################################################################
        //Creat MeetingTimes tab pane
        Tab tabMeetingtimes = csBuilder.buildTab(CS_TAB_MEETINGTIMES, CLASS_TAB);
        ScrollPane MTScrollPane = new ScrollPane();
        VBox MeetingTimePane = csBuilder.buildVBox(CS_MT_PANE, null, CLASS_SITE_PANE, ENABLED);
        
        VBox LecturePane = csBuilder.buildVBox(CS_MT_LEC_PANE,MeetingTimePane, CLASS_SITE_PAGE_PANE, ENABLED);
        HBox LectureHeaderPane = csBuilder.buildHBox(CS_MT_LEC_HEAD_PANE, LecturePane, CLASS_SITE_STYLE_BUTTON,ENABLED );
        Button AddLec = csBuilder.buildTextButton(CS_MT_LEC_ADD_BUTTON, LectureHeaderPane, CLASS_MT_BUTTON, ENABLED);
        Button RemLec = csBuilder.buildTextButton(CS_MT_LEC_REM_BUTTON, LectureHeaderPane, CLASS_MT_BUTTON, ENABLED);
        
        Label MTLectureLabel = csBuilder.buildLabel(CS_MT_LEC_LABEL, LectureHeaderPane, CLASS_BANNER_LABEL_PANE, ENABLED);
        LectureHeaderPane.setSpacing(10);
        TableView lecTable = csBuilder.buildTableView(CS_MT_LEC_TABLE_VIEW, LecturePane, CLASS_OH_TABLE_VIEW, ENABLED);
        //LECTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        TableColumn sectionColumn = csBuilder.buildTableColumn(CS_MT_SEC_TABLE_COLUMN, lecTable, CLASS_OH_COLUMN);
        TableColumn daysColumn = csBuilder.buildTableColumn(CS_MT_DAY_TABLE_COLUMN, lecTable, CLASS_OH_COLUMN);
        TableColumn timeColumn = csBuilder.buildTableColumn(CS_MT_TIME_TABLE_COLUMN, lecTable, CLASS_OH_COLUMN);
        TableColumn roomColumn = csBuilder.buildTableColumn(CS_MT_ROOM_TABLE_COLUMN, lecTable, CLASS_OH_COLUMN);
        sectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        daysColumn.setCellValueFactory(new PropertyValueFactory<String, String>("day"));
        timeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("time"));
        roomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        sectionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        daysColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        timeColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        roomColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        //aaaaa
        sectionColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<LecturePropertype, String>>() {
                @Override
                public void handle(CellEditEvent<LecturePropertype, String> t) {
                  
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     LecturePropertype le = ((LecturePropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = le.getSection();

                      TableView a = t.getTableView();
                     MeetingTime_Transaction transaction = new MeetingTime_Transaction(a,"section",le, oldSection, newSection);
                     app.processTransaction(transaction);
                    
                    //((LecturePropertype) t.getTableView().getItems().get( t.getTablePosition().getRow())).setSection(t.getNewValue());
                }
            }
    );
        daysColumn.setOnEditCommit( new EventHandler<CellEditEvent<LecturePropertype, String>>() {
                @Override
                public void handle(CellEditEvent<LecturePropertype, String> t) {
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     LecturePropertype le = ((LecturePropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = le.getDay();
                     //newLec = oldLec;
                     //newLec.setSection(newSection);
                     //AddLec_Transaction  transaction = new AddLec_Transaction(data, oldLec, newLec);
                      TableView a = t.getTableView();
                     MeetingTime_Transaction transaction = new MeetingTime_Transaction(a,"days",le, oldSection, newSection);
                     app.processTransaction(transaction);
                    
                }
            });
        timeColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<LecturePropertype, String>>() {
                @Override
                public void handle(CellEditEvent<LecturePropertype, String> t) {
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     LecturePropertype le = ((LecturePropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = le.getTime();
                     //System.out.println(newSection);
                      TableView a = t.getTableView();
                     MeetingTime_Transaction transaction = new MeetingTime_Transaction(a,"time",le, oldSection, newSection);
                     app.processTransaction(transaction);
                    //System.out.print(le.getRoom());
                    //((LecturePropertype) t.getTableView().getItems().get( t.getTablePosition().getRow())).setSection(t.getNewValue());
                }
            }
    );
      
        roomColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<LecturePropertype, String>>() {
                @Override
                public void handle(CellEditEvent<LecturePropertype, String> t) {
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     LecturePropertype le = ((LecturePropertype) t.getTableView().getItems().get(t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = le.getRoom();
                     //System.out.println(newSection);
                      TableView a = t.getTableView();
                     MeetingTime_Transaction transaction = new MeetingTime_Transaction(a,"room",le, oldSection, newSection);
                     app.processTransaction(transaction);
                    
                    //((LecturePropertype) t.getTableView().getItems().get( t.getTablePosition().getRow())).setSection(t.getNewValue());
                }
            }
    ); 
        sectionColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        daysColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        timeColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        roomColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        
        VBox ReciPane = csBuilder.buildVBox(CS_MT_REC_PANE,MeetingTimePane, CLASS_SITE_PAGE_PANE, ENABLED);
        HBox RecitationHeaderPane = csBuilder.buildHBox(CS_MT_REC_HEAD_PANE, ReciPane, CLASS_SITE_STYLE_BUTTON,ENABLED );
        Button AddRec = csBuilder.buildTextButton(CS_MT_REC_ADD_BUTTON, RecitationHeaderPane, CLASS_MT_BUTTON, ENABLED);
        Button RemRec = csBuilder.buildTextButton(CS_MT_REC_REM_BUTTON, RecitationHeaderPane, CLASS_MT_BUTTON, ENABLED);
        Label MTRecitationLabel = csBuilder.buildLabel(CS_MT_REC_LABEL, RecitationHeaderPane, CLASS_BANNER_LABEL_PANE, ENABLED);
        RecitationHeaderPane.setSpacing(10);
       
        TableView recTable = csBuilder.buildTableView(CS_MT_REC_TABLE_VIEW, ReciPane, CLASS_OH_TABLE_VIEW, ENABLED);
        //LECTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn recsectionColumn = csBuilder.buildTableColumn(CS_MT_REC_SEC_TABLE_COLUMN, recTable, CLASS_OH_COLUMN);
        TableColumn recdayColumn = csBuilder.buildTableColumn(CS_MT_REC_DAY_TABLE_COLUMN, recTable, CLASS_OH_COLUMN);
        TableColumn recroomColumn = csBuilder.buildTableColumn(CS_MT_REC_ROOM_TABLE_COLUMN, recTable, CLASS_OH_COLUMN);
        TableColumn recta1Column = csBuilder.buildTableColumn(CS_MT_REC_TA1_TABLE_COLUMN, recTable, CLASS_OH_COLUMN);
        TableColumn recta2Column = csBuilder.buildTableColumn(CS_MT_REC_TA2_TABLE_COLUMN, recTable, CLASS_OH_COLUMN);
        
        recsectionColumn.prefWidthProperty().bind(recTable.widthProperty().multiply(1.0 / 5.0));
        recdayColumn.prefWidthProperty().bind(recTable.widthProperty().multiply(1.0 / 5.0));
        recroomColumn.prefWidthProperty().bind(recTable.widthProperty().multiply(1.0 / 5.0));
        recta1Column.prefWidthProperty().bind(recTable.widthProperty().multiply(1.0 / 5.0));
        recta2Column.prefWidthProperty().bind(recTable.widthProperty().multiply(1.0 / 5.0));
        recsectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        recdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("day_time"));
        recta1Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta1"));
        recroomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        recta2Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta2"));
        recsectionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recdayColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        recta1Column.setCellFactory(TextFieldTableCell.forTableColumn());
        recta2Column.setCellFactory(TextFieldTableCell.forTableColumn());
        recroomColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        
         recsectionColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype rec = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = rec.getSection();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"section",rec, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
         recdayColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype rec = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = rec.getDay_time();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"daytime",rec, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
          recta1Column.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype rec = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = rec.getTa1();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"ta1",rec, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
          recta2Column.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype rec = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = rec.getTa2();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"ta2",rec, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
          recroomColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype rec = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = rec.getRoom();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"room",rec, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
          
        VBox LabPane = csBuilder.buildVBox(CS_MT_LAB_PANE,MeetingTimePane, CLASS_SITE_PAGE_PANE, ENABLED);
        HBox LabsHeaderPane = csBuilder.buildHBox(CS_MT_LAB_HEAD_PANE, LabPane, CLASS_SITE_STYLE_BUTTON,ENABLED );
        Button AddLab = csBuilder.buildTextButton(CS_MT_LAB_ADD_BUTTON, LabsHeaderPane, CLASS_MT_BUTTON, ENABLED);
        Button RemLab = csBuilder.buildTextButton(CS_MT_LAB_REM_BUTTON, LabsHeaderPane, CLASS_MT_BUTTON, ENABLED);
        Label MTLabLabel = csBuilder.buildLabel(CS_MT_LAB_LABEL, LabsHeaderPane, CLASS_BANNER_LABEL_PANE, ENABLED);
        LabsHeaderPane.setSpacing(10);
        TableView labTable = csBuilder.buildTableView(CS_MT_LAB_TABLE_VIEW, LabPane, CLASS_OH_TABLE_VIEW, ENABLED);
        //LECTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn labsectionColumn = csBuilder.buildTableColumn(CS_MT_LAB_SEC_TABLE_COLUMN, labTable, CLASS_OH_COLUMN);
        TableColumn labdaysColumn = csBuilder.buildTableColumn(CS_MT_LAB_DAY_TABLE_COLUMN, labTable, CLASS_OH_COLUMN);
        TableColumn labroomColumn = csBuilder.buildTableColumn(CS_MT_LAB_ROOM_TABLE_COLUMN, labTable, CLASS_OH_COLUMN);
        TableColumn labta1Column = csBuilder.buildTableColumn(CS_MT_LAB_TA1_TABLE_COLUMN, labTable, CLASS_OH_COLUMN);
        TableColumn labta2Column = csBuilder.buildTableColumn(CS_MT_LAB_TA2_TABLE_COLUMN, labTable, CLASS_OH_COLUMN);
        labsectionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        labdaysColumn.setCellValueFactory(new PropertyValueFactory<String, String>("day_time"));
        labta1Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta1"));
        labroomColumn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        labta2Column.setCellValueFactory(new PropertyValueFactory<String, String>("ta2"));
        labsectionColumn.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labdaysColumn.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labroomColumn.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labta1Column.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labta2Column.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0 / 5.0));
        labsectionColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        labdaysColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        labta1Column.setCellFactory(TextFieldTableCell.forTableColumn());
        labta2Column.setCellFactory(TextFieldTableCell.forTableColumn());
        labroomColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        labsectionColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype lab = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = lab.getSection();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"section",lab, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
        labdaysColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype lab = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = lab.getDay_time();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"daytime",lab, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
        labta2Column.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype lab = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = lab.getTa2();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"ta2",lab, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
        labta1Column.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype lab = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = lab.getTa1();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"ta1",lab, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
        labroomColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<RecLabPropertype, String>>() {
                @Override
                public void handle(CellEditEvent<RecLabPropertype, String> t) {           
                    CourseSiteData data = (CourseSiteData) app.getDataComponent();
                     RecLabPropertype lab = ((RecLabPropertype) t.getTableView().getItems().get( t.getTablePosition().getRow()));
                     newSection = t.getNewValue();
                     oldSection = lab.getRoom();
                     TableView a = t.getTableView();
                     MeetingTimeRL_Transaction transaction = new MeetingTimeRL_Transaction(a,"room",lab, oldSection, newSection);
                     app.processTransaction(transaction);  
                }
            }
    );
        

        MeetingTimePane.setSpacing(10);
        MTScrollPane.setContent(MeetingTimePane);
        tabMeetingtimes.setContent(MTScrollPane);
         //################################################################################################
        //Creat OfficeHours tab pane
        Tab tabOfficeHours = csBuilder.buildTab(CS_TAB_OFFICEHOURS, CLASS_TAB);
        
        VBox leftPane = csBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        HBox tasHeaderBox = csBuilder.buildHBox(OH_TAS_HEADER_PANE, leftPane, CLASS_OH_BOX, ENABLED);
        Button removeTaButton = csBuilder.buildTextButton(OH_TAS_REM_BUTTON, tasHeaderBox, CLASS_MT_BUTTON, ENABLED);
        
        csBuilder.buildLabel(CourseSitePropertyType.OH_TAS_HEADER_LABEL, tasHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);
        HBox typeHeaderBox = csBuilder.buildHBox(OH_GRAD_UNDERGRAD_TAS_PANE, tasHeaderBox, CLASS_OH_RADIO_BOX, ENABLED);
        ToggleGroup tg = new ToggleGroup();
        csBuilder.buildRadioButton(OH_ALL_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, true);
        csBuilder.buildRadioButton(OH_GRAD_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, false);
        csBuilder.buildRadioButton(OH_UNDERGRAD_RADIO_BUTTON, typeHeaderBox, CLASS_OH_RADIO_BUTTON, ENABLED, tg, false);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        TableView<TeachingAssistantPrototype> taTable = csBuilder.buildTableView(OH_TAS_TABLE_VIEW, leftPane, CLASS_OH_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        TableColumn nameColumn = csBuilder.buildTableColumn(OH_NAME_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn emailColumn = csBuilder.buildTableColumn(OH_EMAIL_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        TableColumn slotsColumn = csBuilder.buildTableColumn(OH_SLOTS_TABLE_COLUMN, taTable, CLASS_OH_CENTERED_COLUMN);
        TableColumn typeColumn = csBuilder.buildTableColumn(OH_TYPE_TABLE_COLUMN, taTable, CLASS_OH_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("slots"));
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(2.0 / 5.0));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0 / 5.0));
         // ADD BOX FOR ADDING A TA
        HBox taBox = csBuilder.buildHBox(OH_ADD_TA_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        csBuilder.buildTextField(OH_NAME_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        csBuilder.buildTextField(OH_EMAIL_TEXT_FIELD, taBox, CLASS_OH_TEXT_FIELD, ENABLED);
        csBuilder.buildTextButton(OH_ADD_TA_BUTTON, taBox, CLASS_OH_BUTTON, !ENABLED);
        
        HBox officeHoursHeaderBox = csBuilder.buildHBox(OH_OFFICE_HOURS_HEADER_PANE, leftPane, CLASS_OH_PANE, ENABLED);
        csBuilder.buildLabel(OH_OFFICE_HOURS_HEADER_LABEL, officeHoursHeaderBox, CLASS_OH_HEADER_LABEL, ENABLED);

        // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = csBuilder.buildTableView(OH_OFFICE_HOURS_TABLE_VIEW, leftPane, CLASS_OH_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        setupOfficeHoursColumn(OH_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN, "startTime");
        setupOfficeHoursColumn(OH_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_OH_TIME_COLUMN, "endTime");
        setupOfficeHoursColumn(OH_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "monday");
        setupOfficeHoursColumn(OH_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "tuesday");
        setupOfficeHoursColumn(OH_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "wednesday");
        setupOfficeHoursColumn(OH_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "thursday");
        setupOfficeHoursColumn(OH_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_OH_DAY_OF_WEEK_COLUMN, "friday");
        
        HBox OHComboBox = csBuilder.buildHBox(OH_COMBO_BOX, officeHoursHeaderBox, CLASS_OH_RADIO_BOX, ENABLED);
        Label StartTime = csBuilder.buildLabel(OH_START_COMBO_LABEL, OHComboBox, CLASS_OH_HEADER_LABEL, ENABLED);
        ComboBox StartHourCombo = csBuilder.buildComboBox(OH_START_COMBO,"" ,"" , OHComboBox, CLASS_BANNER_COMBO_PANE, ENABLED);
        Label EndTime = csBuilder.buildLabel(OH_END_COMBO_LABEL, OHComboBox, CLASS_OH_HEADER_LABEL, ENABLED);
        StartHourCombo.getItems().addAll("9:00am","9:30am","10:00am","10:30am","11:00am","11:30am","12:00pm","12:30pm","1:00pm","1:30pm","2:00pm","2:30pm","3:00pm","3:30pm","4:00pm","4:30pm","5:00pm","5:30pm","6:00pm","6:30pm","7:00pm","7:30pm","8:00pm","8:30pm");
        StartHourCombo.getSelectionModel().selectFirst();
        ComboBox EndHourCombo = csBuilder.buildComboBox(OH_END_COMBO, "", "", OHComboBox, CLASS_BANNER_COMBO_PANE, ENABLED);
        EndHourCombo.getItems().addAll("9:30am","10:00am","10:30am","11:00am","11:30am","12:00pm","12:30pm","1:00pm","1:30pm","2:00pm","2:30pm","3:00pm","3:30pm","4:00pm","4:30pm","5:00pm","5:30pm","6:00pm","6:30pm","7:00pm","7:30pm","8:00pm","8:30pm");
        EndHourCombo.getSelectionModel().selectLast();
        tabOfficeHours.setContent(leftPane);
         //################################################################################################
        //Creat Schedule tab pane
        Tab tabSchedule = csBuilder.buildTab(CS_TAB_SCHEDULE, CLASS_TAB);
        ScrollPane SchScrollPane = new ScrollPane();
        VBox SchPane = csBuilder.buildVBox(CS_SCH_PANE, null, CLASS_SITE_PANE, ENABLED);
        VBox calenderSchPane = csBuilder.buildVBox(CS_SCH_CAL_PANE, SchPane, CLASS_SITE_PAGE_PANE, ENABLED);
        Label CalBond = csBuilder.buildLabel(CS_SCH_CAL_LABEL, calenderSchPane, CLASS_BANNER_LABEL_PANE, ENABLED);
        HBox starHBoxPane = csBuilder.buildHBox(CS_SCH_STA_PANE, calenderSchPane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Label startMon = csBuilder.buildLabel(CS_SCH_STA_MON_LABEL, starHBoxPane, CLASS_BANNER_LABEL_PANE, ENABLED);
        DatePicker datePicker = csBuilder.buildDate(CS_SCH_DATE_PICKER, starHBoxPane, CLASS_TAB, ENABLED);
       
        
        //starHBoxPane.getChildren().add(datePicker1);
        //ComboBox startMonCombo = csBuilder.buildComboBox(CS_SCH_STA_MON_COM, outcomesTitle, EmailTextField, starHBoxPane, CLASS_BANNER_COMBO_PANE, ENABLED);
        Label endFri = csBuilder.buildLabel(CS_SCH_END_FRI_LABEL, starHBoxPane, CLASS_BANNER_LABEL_PANE, ENABLED);
        DatePicker datePicker2 = csBuilder.buildDate(CS_SCH_DATE_PICKER2, starHBoxPane, CLASS_TAB, ENABLED);
        //DatePicker datePicker2 = new DatePicker();
        //starHBoxPane.getChildren().add(datePicker2);
        //ComboBox endMonCombo = csBuilder.buildComboBox(CS_SCH_END_FRI_COM, outcomesTitle, EmailTextField, starHBoxPane, CLASS_BANNER_COMBO_PANE, ENABLED);
        starHBoxPane.setSpacing(30);
        calenderSchPane.setSpacing(10);
        
        
        VBox scheduleItemPane = csBuilder.buildVBox(CS_SCH_SCH_ITEM_PANE, SchPane, CLASS_SITE_PAGE_PANE, ENABLED);
        HBox scheduleItemHBoxPane = csBuilder.buildHBox(CS_SCH_ITEM_PANE, scheduleItemPane, CLASS_SITE_STYLE_BUTTON, ENABLED);
        Button scheduleRemButton = csBuilder.buildTextButton(CS_SCH_REM_BUTTON, scheduleItemHBoxPane, CLASS_MT_BUTTON, ENABLED);
        Label SchItem = csBuilder.buildLabel(CS_SCH_ITEM_LABEL, scheduleItemHBoxPane, CLASS_BANNER_LABEL_PANE, ENABLED);
        
        TableView SchItemTable = csBuilder.buildTableView(CS_SCH_TABLE_VIEW, scheduleItemPane, CLASS_OH_TABLE_VIEW, ENABLED);
        TableColumn schtypeColumn = csBuilder.buildTableColumn(CS_SCH_TYPE_TABLE_COLUMN, SchItemTable, CLASS_OH_COLUMN);
        TableColumn dateColumn = csBuilder.buildTableColumn(CS_SCH_DATE_TABLE_COLUMN, SchItemTable, CLASS_OH_COLUMN);
        TableColumn titleColumn = csBuilder.buildTableColumn(CS_SCH_TITLE_TABLE_COLUMN, SchItemTable, CLASS_OH_COLUMN);
        TableColumn topicColumn = csBuilder.buildTableColumn(CS_SCH_TOPIC_TABLE_COLUMN, SchItemTable, CLASS_OH_COLUMN);
        schtypeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<String, String>("date"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<String, String>("title"));
        topicColumn.setCellValueFactory(new PropertyValueFactory<String, String>("topic"));
        schtypeColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        dateColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        titleColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        topicColumn.prefWidthProperty().bind(lecTable.widthProperty().multiply(1.0 / 4.0));
        scheduleItemPane.setSpacing(10);
        GridPane addoreditPane = csBuilder.buildGridPane(CS_SCH_ADDORED_PANE, SchPane, CLASS_SITE_PAGE_PANE, ENABLED);
        Label addoreditLabel = csBuilder.buildLabel(CS_SCH_ADDORED_LABEL, addoreditPane,0,0,1,1, CLASS_BANNER_LABEL_PANE, ENABLED);
        Label typeSchLabel = csBuilder.buildLabel(CS_SCH_TYPE_LABEL, addoreditPane, 0, 1, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
        ComboBox typeSchCombo = csBuilder.buildComboBox(CS_SCH_TYPE_COMBO, addoreditPane, 1, 1, 1, 1, CLASS_BANNER_COMBO_PANE, ENABLED, outcomesTitle, EmailTextField);
        typeSchCombo.getItems().addAll("holidays","lectures","hws","recitations","labs","references");
        typeSchCombo.getSelectionModel().selectFirst();
        Label dateSchLabel = csBuilder.buildLabel(CS_SCH_DATE_LABEL, addoreditPane, 0, 2, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
        DatePicker datePicker3 = csBuilder.buildDate(CS_SCH_DATE_PICKER3, addoreditPane,1,2,1,1, CLASS_TAB, ENABLED);
        datePicker3.setDisable(ENABLED);
       // ComboBox dateSchCombo = csBuilder.buildComboBox(CS_SCH_DATE_COMBO, addoreditPane, 1, 2, 1, 1, CLASS_BANNER_COMBO_PANE, ENABLED, outcomesTitle, EmailTextField);
        Label titleSchLabel = csBuilder.buildLabel(CS_SCH_TITLE_LABEL, addoreditPane, 0, 3, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
        TextField titleSchTextField = csBuilder.buildTextField(CS_SCH_TITLE_TEXTFIELD, addoreditPane,1,3,1,1, CLASS_SITE_TEXTFIELD, ENABLED);
        Label topicSchLabel = csBuilder.buildLabel(CS_SCH_TOPIC_LABEL, addoreditPane, 0, 4, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
        TextField topicSchTextField = csBuilder.buildTextField(CS_SCH_TOPIC_TEXTFIELD, addoreditPane,1,4,1,1, CLASS_SITE_TEXTFIELD, ENABLED);
        Label linkSchLabel = csBuilder.buildLabel(CS_SCH_LINK_LABEL, addoreditPane, 0, 5, 1, 1, CLASS_BANNER_LABEL_PANE, ENABLED);
        TextField linkSchTextField = csBuilder.buildTextField(CS_SCH_LINK_TEXTFIELD, addoreditPane,1,5,1,1, CLASS_SITE_TEXTFIELD, ENABLED);
        Button scheduleaddButton = csBuilder.buildTextButton(CS_SCH_ADD_BUTTON, addoreditPane,0,6,1,1, CLASS_MT_BUTTON, ENABLED);
        Button scheduleclearButton = csBuilder.buildTextButton(CS_SCH_CLEAR_BUTTON, addoreditPane,1,6,1,1,CLASS_MT_BUTTON, ENABLED);
        //scheduleaddButton.setDisable(ENABLED);
        addoreditPane.setHgap(10);
        addoreditPane.setVgap(10);
        
        SchPane.setSpacing(10);
         SchScrollPane.setContent(SchPane);
        tabSchedule.setContent(SchScrollPane);
         //################################################################################################
        layout.getTabs().add(tabSite);
        layout.getTabs().add(tabSyllabus);
        layout.getTabs().add(tabMeetingtimes);
        layout.getTabs().add(tabOfficeHours);
        layout.getTabs().add(tabSchedule);
        layout.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        
         workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(layout);
        layout.tabMinWidthProperty().bind(((BorderPane)workspace).widthProperty().divide(layout.getTabs().size()).subtract(20));
        sitePane.minWidthProperty().bind(((BorderPane)workspace).widthProperty());
        syllabusPane.minWidthProperty().bind(((BorderPane)workspace).widthProperty());
        MeetingTimePane.minWidthProperty().bind(((BorderPane)workspace).widthProperty());
        SchPane.minWidthProperty().bind(((BorderPane)workspace).widthProperty());
     }

     private void setupOfficeHoursColumn(Object columnId, TableView tableView, String styleClass, String columnDataProperty) {
        AppNodesBuilder builder = app.getGUIModule().getNodesBuilder();
        TableColumn<TeachingAssistantPrototype, String> column = builder.buildTableColumn(columnId, tableView, styleClass);
        column.setCellValueFactory(new PropertyValueFactory<TeachingAssistantPrototype, String>(columnDataProperty));
        column.prefWidthProperty().bind(tableView.widthProperty().multiply(1.0 / 7.0));
        column.setCellFactory(col -> {
            return new TableCell<TeachingAssistantPrototype, String>() {
                @Override
                protected void updateItem(String text, boolean empty) {
                    super.updateItem(text, empty);
                    if (text == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        // CHECK TO SEE IF text CONTAINS THE NAME OF
                        // THE CURRENTLY SELECTED TA
                        setText(text);
                        TableView<TeachingAssistantPrototype> tasTableView = (TableView) app.getGUIModule().getGUINode(OH_TAS_TABLE_VIEW);
                        TeachingAssistantPrototype selectedTA = tasTableView.getSelectionModel().getSelectedItem();
                        if (selectedTA == null) {
                            setStyle("");
                        } else if (text.contains(selectedTA.getName())) {
                            setStyle("-fx-background-color: yellow");
                        } else {
                            setStyle("");
                        }
                    }
                }
            };
        });
    }
        public void initControllers() {
        CourseSiteController controller = new CourseSiteController((CourseSiteGeneratorApp) app);
        AppGUIModule gui = app.getGUIModule();
       
        // FOOLPROOF DESIGN STUFF
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));
        Button removeTaButton = ((Button) gui.getGUINode(OH_TAS_REM_BUTTON));
        TextField titleTextField = ((TextField) gui.getGUINode(CS_SITE_TITLE_TF));
        TextField instruNameTextField = ((TextField)gui.getGUINode(CS_SITE_NAME_TEXTFIELD));
        TextField instruRoomTextField = ((TextField)gui.getGUINode(CS_SITE_ROOM_TEXTFIELD));
        TextField instruEmailTextField = ((TextField)gui.getGUINode(CS_SITE_EMAIL_TEXTFIELD));
        TextField instruHPTextField = ((TextField)gui.getGUINode(CS_SITE_HOMEPAGE_TEXTFIELD));
        
        ComboBox officeHourStart = ((ComboBox)gui.getGUINode(OH_START_COMBO));
        ComboBox officeHourEnd = ((ComboBox)gui.getGUINode(OH_END_COMBO));
        
        ComboBox Subject_combo =((ComboBox)gui.getGUINode(CS_SITE_SUBJECT_COMBOBOX));
        ComboBox Number_combo =((ComboBox)gui.getGUINode(CS_SITE_NUMBER_COMBOBOX));
        ComboBox Semester_combo =((ComboBox)gui.getGUINode(CS_SITE_SEMESTER_COMBOBOX));
        ComboBox Year_combo = ((ComboBox)gui.getGUINode(CS_SITE_YEAR_COMBOBOX));
        
        Button RightFooterButton =((Button)gui.getGUINode(SITE_STYLE_RIGHTFOOT_BUTTON));
        Button NavbarButton = ((Button)gui.getGUINode(SITE_STYLE_NAVBAR_BUTTON));
        Button FaviconButton = ((Button)gui.getGUINode(SITE_STYLE_FAVCON_BUTTON));
        Button LeftFooterButton = ((Button)gui.getGUINode(SITE_STYLE_LEFTFOOT_BUTTON));
        ComboBox StyleSheet_combo = ((ComboBox)gui.getGUINode(SITE_STYLE_COMBO));
        Button add_lecButton = ((Button)gui.getGUINode(CS_MT_LEC_ADD_BUTTON));
        Button add_recButton = ((Button)gui.getGUINode(CS_MT_REC_ADD_BUTTON));   
        Button add_labButton = ((Button)gui.getGUINode(CS_MT_LAB_ADD_BUTTON));
        //Syllabus Pane thing??????????????????????????????????????????????????????
        TextArea des_ta = ((TextArea)gui.getGUINode(CS_SYLL_DES_TA_PANE));
        TextArea topic_ta = ((TextArea)gui.getGUINode(CS_STYLL_TOPIC_TA));
        TextArea prere_ta = ((TextArea)gui.getGUINode(CS_STYLL_PREREQUIST_TA));
        TextArea outcome_ta = ((TextArea)gui.getGUINode(CS_STYLL_OUTCOME_TA));
        TextArea tb_ta = ((TextArea)gui.getGUINode(CS_STYLL_TEXTBOOK_TA));
        TextArea gradecom_ta = ((TextArea)gui.getGUINode(CS_STYLL_GRADEDCOMP_TA));
        TextArea gradenote_ta = ((TextArea)gui.getGUINode(CS_STYLL_GRADENOTE_TA));
        TextArea acdamic_ta = ((TextArea)gui.getGUINode(CS_STYLL_ACADEMIC_TITLE_TA));
        TextArea spass_ta = ((TextArea)gui.getGUINode(CS_STYLL_SPEASS_TITLE_TA));
        TextArea instru_oh = ((TextArea)gui.getGUINode(CS_SITE_OFFICE_TA_PANE));
        
        ComboBox option = ((ComboBox)gui.getGUINode(CS_SCH_TYPE_COMBO));
        DatePicker datePicker1 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER));
        DatePicker datePicker2 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER2));
        DatePicker datePicker3 = ((DatePicker)gui.getGUINode(CS_SCH_DATE_PICKER3));
        Button addorupdate = ((Button)gui.getGUINode(CS_SCH_ADD_BUTTON));
        Button clear = ((Button)gui.getGUINode(CS_SCH_CLEAR_BUTTON));
        TextField title = ((TextField)gui.getGUINode(CS_SCH_TITLE_TEXTFIELD));
        TextField topic = ((TextField)gui.getGUINode(CS_SCH_TOPIC_TEXTFIELD));
        TableView schTable = ((TableView)gui.getGUINode(CS_SCH_TABLE_VIEW));
        TextField link = ((TextField)gui.getGUINode(CS_SCH_LINK_TEXTFIELD));
        Button remSch = ((Button)gui.getGUINode(CS_SCH_REM_BUTTON));
        CheckBox homeBox = ((CheckBox)gui.getGUINode(CS_SITE_HOME_CHECKBOX));
        CheckBox SyllabusBox = ((CheckBox)gui.getGUINode(CS_SITE_SYLLABUS_CHECKBOX));
        CheckBox ScheduleBox = ((CheckBox)gui.getGUINode(CS_SITE_SCHEDULE_CHECKBOX));
        CheckBox HwsBox = ((CheckBox)gui.getGUINode(CS_SITE_HWS_CHECKBOX));
         AppNodesBuilder csBuilder = app.getGUIModule().getNodesBuilder();
        HwsBox.focusedProperty().addListener(e->{
             if(!HwsBox.isFocused()){
                 newsyState = HwsBox.isSelected();
                 controller.processEditCheckBox("hws",oldsyState, newsyState);
                 controller.processCheckCheckBox();
             }else{
                 oldsyState = HwsBox.isSelected();
             } 
        });
        ScheduleBox.focusedProperty().addListener(e->{
             if(!ScheduleBox.isFocused()){
                 newsyState = ScheduleBox.isSelected();
                 controller.processEditCheckBox("schedule",oldsyState, newsyState);
                 controller.processCheckCheckBox();
             }else{
                 oldsyState = ScheduleBox.isSelected();
             } 
        });
        SyllabusBox.focusedProperty().addListener(e->{
           if(!SyllabusBox.isFocused()){
                 newsyState = SyllabusBox.isSelected();
                 controller.processEditCheckBox("syllabus",oldsyState, newsyState);
                 controller.processCheckCheckBox();
             }else{
                 oldsyState = SyllabusBox.isSelected();
             } 
        });
        homeBox.focusedProperty().addListener(e->{
             if(!homeBox.isFocused()){
                 newState = homeBox.isSelected();
                 controller.processEditCheckBox("home",oldState, newState);
                 controller.processCheckCheckBox();
             }else{
                 oldState = homeBox.isSelected();
             }
             
         });
        datePicker1.focusedProperty().addListener(e->{
             if(!datePicker1.isFocused()){
                newTextValue = datePicker1.getValue().toString();
                controller.processChangeDate("start",oldTextValue,newTextValue);
                controller.processCheckStartEnd();
            }else{
                if(datePicker1.getValue() == null){
                    oldTextValue = "";
                }else{
                    oldTextValue = datePicker1.getValue().toString();
                }
            }
            
        });
        datePicker2.focusedProperty().addListener(e->{
            if(!datePicker2.isFocused()){
                newTextValue = datePicker2.getValue().toString();
                controller.processChangeDate("end",oldTextValue,newTextValue);
                controller.processCheckStartEnd();
            }else{
                if(datePicker2.getValue() == null){
                    oldTextValue = "";
                }else{
                    oldTextValue = datePicker2.getValue().toString();
                }
            }
            
        });
        schTable.getSelectionModel().selectedItemProperty().addListener(e->{
            csBuilder.changeLabeledNode(CS_SCH_ADD_BUTTON,addorupdate,"update");
            controller.processTransDown();
        });
        clear.setOnAction(e->{
           csBuilder.changeLabeledNode(CS_SCH_ADD_BUTTON,addorupdate,"add");
           option.getSelectionModel().selectFirst();
           datePicker3.getEditor().clear();
           title.clear();
           topic.clear();
           link.clear();
        });
        remSch.setOnAction(e->{
           controller.processRemSch();
           option.getSelectionModel().selectFirst();
           datePicker3.setValue(null);
           title.clear();
           topic.clear();
           link.clear(); 
        });
        option.getSelectionModel().selectedItemProperty().addListener(e->{
            controller.processcheckSchduleItem();
        });
        datePicker3.valueProperty().addListener(e->{
            controller.processcheckSchduleItem();          
        });
        title.textProperty().addListener(e->{
             controller.processcheckSchduleItem();
        });
        topic.textProperty().addListener(e->{
            controller.processcheckSchduleItem();
        });
        addorupdate.setOnAction(e->{
            if(addorupdate.getText().equals("Update")){
                controller.processUpdateScheduleItem();
                option.getSelectionModel().selectFirst();
                datePicker3.setValue(null);
                title.clear();
                topic.clear();
                link.clear();
            }else{ 
                controller.processSchduleItem();
                 option.getSelectionModel().selectFirst();
                datePicker3.setValue(null);
                title.clear();
                topic.clear();
                link.clear();
            }
        });
        //syllabus pane????????????????????????????????????????????????????????????
        spass_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!spass_ta.isFocused()){
                newTextValue = spass_ta.getText();
                controller.processChangeSyllabus("specialAssistance",oldTextValue,newTextValue);
            }else{
                oldTextValue = spass_ta.getText();
            }
        });
        
        acdamic_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!acdamic_ta.isFocused()){
                newTextValue = acdamic_ta.getText();
                controller.processChangeSyllabus("academicDishonesty",oldTextValue,newTextValue);
            }else{
                oldTextValue = acdamic_ta.getText();
            }
        });
        
        gradenote_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!gradenote_ta.isFocused()){
                newTextValue = gradenote_ta.getText();
                controller.processChangeSyllabus("gradingNote",oldTextValue,newTextValue);
            }else{
                oldTextValue = gradenote_ta.getText();
            }
        });
        gradecom_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!gradecom_ta.isFocused()){
                newTextValue = gradecom_ta.getText();
                controller.processChangeSyllabus("gradedComponents",oldTextValue,newTextValue);
            }else{
                oldTextValue = gradecom_ta.getText();
            }
        });
        tb_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!tb_ta.isFocused()){
                newTextValue = tb_ta.getText();
                controller.processChangeSyllabus("textbooks",oldTextValue,newTextValue);
            }else{
                oldTextValue = tb_ta.getText();
            }
        });
        outcome_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!outcome_ta.isFocused()){
                newTextValue = outcome_ta.getText();
                controller.processChangeSyllabus("outcomes",oldTextValue,newTextValue);
            }else{
                oldTextValue = outcome_ta.getText();
            }
        });
        prere_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!prere_ta.isFocused()){
                newTextValue = prere_ta.getText();
                controller.processChangeSyllabus("prerequisites",oldTextValue,newTextValue);
            }else{
                oldTextValue = prere_ta.getText();
            }
        });
        topic_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!topic_ta.isFocused()){
                newTextValue = topic_ta.getText();
                controller.processChangeSyllabus("topics",oldTextValue,newTextValue);
            }else{
                oldTextValue = topic_ta.getText();
            }
        });
        des_ta.focusedProperty().addListener((Observable, oldValue, newValue)->{
            if(!des_ta.isFocused()){
                newTextValue = des_ta.getText();
                controller.processChangeSyllabus("description", oldTextValue, newTextValue);
            }else{
                oldTextValue = des_ta.getText();
            }
            
        });
        
        //给lec加一行？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？
        add_lecButton.setOnAction(e ->{
            controller.processAddinitLec();
        });
        add_recButton.setOnAction(e->{
            controller.processAddinitRecLab("rec");
        });
        add_labButton.setOnAction(e->{
           controller.processAddinitRecLab("lab"); 
        });
        //加载图片？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？
        FaviconButton.setOnAction(e->{
              FileChooser fc = new FileChooser();
              File selectedFile = fc.showOpenDialog(app.getGUIModule().getWindow());
              String filePath = selectedFile.getAbsolutePath();
               String [] a = filePath.split("/");
           filePath = "./"+a[(a.length)-2]+"/"+a[a.length-1];
              newFilePath = filePath;
             //System.out.print("file:"+filePath);
              //ImageView iv1 = ((ImageView)gui.getGUINode(SITE_FAV_ICON));
              controller.processChangeImage(oldFilePath, newFilePath,"fav");
              oldFilePath = newFilePath;
              //Image image1 = new Image("file:"+filePath);
              //iv1.setImage(image1);
             //controller.setString("Fav",filePath);
         });
        NavbarButton.setOnAction(e->{
            FileChooser fc = new FileChooser();
            File selectedFile = fc.showOpenDialog(app.getGUIModule().getWindow());
           String filePath = selectedFile.getPath();
           String [] a = filePath.split("/");
           filePath = "./"+a[(a.length)-2]+"/"+a[a.length-1];
          // System.out.println("./"+a[(a.length)-2]+"/"+a[a.length-1]);
            newnavPath = filePath;
            controller.processChangeImage(oldnavPath, newnavPath,"nav");
            oldnavPath = newnavPath;
             //System.out.print("file:"+filePath);
              //ImageView iv2 = ((ImageView)gui.getGUINode(SITE_STYLE_ICON));
              //Image image2 = new Image("file:"+filePath);
              //iv2.setImage(image2);
              //controller.setString("Nav",filePath);
        });
        LeftFooterButton.setOnAction(e->{
            FileChooser fc = new FileChooser();
            File selectedFile = fc.showOpenDialog(app.getGUIModule().getWindow());
            String filePath = selectedFile.getAbsolutePath();
             String [] a = filePath.split("/");
           filePath = "./"+a[(a.length)-2]+"/"+a[a.length-1];
            newleftPath = filePath;
            controller.processChangeImage(oldleftPath, newleftPath,"left");
            oldleftPath = newleftPath;
             //System.out.print("file:"+filePath);
//              ImageView iv3 = ((ImageView)gui.getGUINode(SITE_STYLE_LEFT_ICON));
//              Image image3 = new Image("file:"+filePath);
//              iv3.setImage(image3);
//              controller.setString("Left",filePath);

        });
        RightFooterButton.setOnAction(e->{
            FileChooser fc = new FileChooser();
            File selectedFile = fc.showOpenDialog(app.getGUIModule().getWindow());
            String filePath = selectedFile.getAbsolutePath();
             String [] a = filePath.split("/");
           filePath = "./"+a[(a.length)-2]+"/"+a[a.length-1];
            newrightPath = filePath;
            controller.processChangeImage(oldrightPath, newrightPath,"right");
            oldrightPath = newrightPath;
//             //System.out.print("file:"+filePath);
//              ImageView iv4 = ((ImageView)gui.getGUINode(SITE_STYLE_RIGHT_ICON));
//              Image image4 = new Image("file:"+filePath);
//              iv4.setImage(image4);
//              controller.setString("Right", filePath);

        });
        //STYLESHEET COMBO Box??????????????????????????????????????????????????
        instruNameTextField.focusedProperty().addListener(e->{
            if(!instruNameTextField.isFocused()){
                newTextValue = instruNameTextField.getText().trim();
                controller.processAddToInstru("name",oldTextValue,newTextValue);
            }else{
                oldTextValue = instruNameTextField.getText().trim();
            }
        });
        instruRoomTextField.focusedProperty().addListener(e->{
            if(!instruRoomTextField.isFocused()){
                newTextValue = instruRoomTextField.getText().trim();
                controller.processAddToInstru("room",oldTextValue,newTextValue);
            }else{
                oldTextValue = instruRoomTextField.getText().trim();
            }
        });
        instruEmailTextField.focusedProperty().addListener(e->{
            if(!instruEmailTextField.isFocused()){
                newTextValue = instruEmailTextField.getText().trim();
                controller.processAddToInstru("email",oldTextValue,newTextValue);
            }else{
                oldTextValue = instruEmailTextField.getText().trim();
            }
        });
        instruHPTextField.focusedProperty().addListener(e->{
            if(!instruHPTextField.isFocused()){
                newTextValue = instruHPTextField.getText().trim();
                controller.processAddToInstru("homepage",oldTextValue,newTextValue);
            }else{
                oldTextValue = instruHPTextField.getText().trim();
            }
        });
        instru_oh.focusedProperty().addListener(e->{
             if(!instru_oh.isFocused()){
                newTextValue = instru_oh.getText();
                controller.processAddToInstru("oh",oldTextValue,newTextValue);
            }else{
                oldTextValue = instru_oh.getText();
            }
        });
        
        titleTextField.focusedProperty().addListener(e->{
              if(!titleTextField.isFocused()){
              newTextValue = titleTextField.getText().trim();
              controller.processAddtitle("title",oldTextValue, newTextValue);
              }else{
                  oldTextValue = titleTextField.getText().trim();
              }
          });
          
          Subject_combo.focusedProperty().addListener(e->{
              try {
                controller.processAddTocombo();
                controller.processChangeSiteCombo();
            } catch (IOException ex) {
                Logger.getLogger(CourseSiteWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
              if(!Subject_combo.isFocused()){
                  if(oldTextValue != newTextValue){
                newTextValue = (String) Subject_combo.getSelectionModel().getSelectedItem();
                controller.processChangeCombo("subject",oldTextValue,newTextValue);
                  }
            }else{
                oldTextValue = (String)  Subject_combo.getSelectionModel().getSelectedItem();
            }
            
        });
          StyleSheet_combo.setOnMouseClicked(e->{                            
               File repo = new File ("./work");
               
               File[] fileList = repo.listFiles();
               for(int i = 0; i < fileList.length; i++){
                  String name = fileList[i].getName();
                   if(name.contains(".css")&&!(StyleSheet_combo.getItems().contains(name))){
                       StyleSheet_combo.getItems().add(name);
                   }
               }
               StyleSheet_combo.getSelectionModel().selectFirst();
          });
         
          StyleSheet_combo.focusedProperty().addListener(e->{
          
              if(!StyleSheet_combo.isFocused()){
                newTextValue = (String) StyleSheet_combo.getSelectionModel().getSelectedItem();
                controller.processChangeCombo("style",oldTextValue,newTextValue);
                File afile = new File("./work/"+StyleSheet_combo.getSelectionModel().getSelectedItem().toString());
                File bfile = new File("./export/sample/css/"+afile.getName());
           
                  try {
                      FileUtils.copyFile(afile, bfile);
                      //file.renameTo(new File("./export/css/"+file.getName()));
                  } catch (IOException ex) {
                      Logger.getLogger(CourseSiteWorkspace.class.getName()).log(Level.SEVERE, null, ex);
                  }
                bfile.renameTo(new File("./export/sample/css/sea_wolf.css"));
            }else{
                oldTextValue = (String)  StyleSheet_combo.getSelectionModel().getSelectedItem();
            }
          });
          Semester_combo.focusedProperty().addListener(e->{
             controller.processChangeSiteCombo();
              if(!Semester_combo.isFocused()){
                newTextValue = (String) Semester_combo.getSelectionModel().getSelectedItem();
                controller.processChangeCombo("semester",oldTextValue,newTextValue);
                
            }else{
                oldTextValue = (String)  Semester_combo.getSelectionModel().getSelectedItem();
            }
        });
          
          Year_combo.focusedProperty().addListener(e->{
            controller.processChangeSiteCombo();
            if(!Year_combo.isFocused()){
                newTextValue = (String) Year_combo.getSelectionModel().getSelectedItem();
                controller.processChangeCombo("year",oldTextValue,newTextValue);
               
            }else{
                oldTextValue = (String)  Year_combo.getSelectionModel().getSelectedItem();
            }
        });
        Number_combo.focusedProperty().addListener(e ->{
            try {
              
                controller.processAddTocombo();
                controller.processChangeSiteCombo();

               
            } catch (IOException ex) {
                Logger.getLogger(CourseSiteWorkspace.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(!Number_combo.isFocused()){
                newTextValue = (String) Number_combo.getSelectionModel().getSelectedItem();
                controller.processChangeCombo("number",oldTextValue,newTextValue);
               
            }else{
                oldTextValue = (String)  Number_combo.getSelectionModel().getSelectedItem();
            }
        });
     //office hour????????????????????????????????????????????????????????????????
       officeHourStart.getSelectionModel().selectedItemProperty().addListener(e ->{
           controller.processCheckTime();
       });
         officeHourEnd.getSelectionModel().selectedItemProperty().addListener(e ->{
           controller.processCheckTime();
       });
        removeTaButton.setOnAction(e ->{
            controller.processCutTA();
        });
        nameTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });
        emailTextField.textProperty().addListener(e -> {
            controller.processTypeTA();
        });

        // FIRE THE ADD EVENT ACTION
        nameTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        emailTextField.setOnAction(e -> {
            controller.processAddTA();
        });
        ((Button) gui.getGUINode(OH_ADD_TA_BUTTON)).setOnAction(e -> {
            controller.processAddTA();
        });

        TableView officeHoursTableView = (TableView) gui.getGUINode(OH_OFFICE_HOURS_TABLE_VIEW);
        officeHoursTableView.getSelectionModel().setCellSelectionEnabled(true);
        officeHoursTableView.setOnMouseClicked(e -> {
            controller.processToggleOfficeHours();
        });
        //lec rec lab remove button
        Button remLec = (Button)gui.getGUINode(CS_MT_LEC_REM_BUTTON);
        Button remRec = (Button)gui.getGUINode(CS_MT_REC_REM_BUTTON);
        Button remLab = (Button)gui.getGUINode(CS_MT_LAB_REM_BUTTON);
        remLec.setOnAction(e->{
            controller.processRemLec();
          });
        remRec.setOnAction(e->{
            controller.processRemRecLab("rec");
        });
        remLab.setOnAction(e->{
            controller.processRemRecLab("lab");
        });
      
        

        // DON'T LET ANYONE SORT THE TABLES
        TableView tasTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn) officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        for (int i = 0; i < tasTableView.getColumns().size(); i++) {
            ((TableColumn) tasTableView.getColumns().get(i)).setSortable(false);
        }

        tasTableView.setOnMouseClicked(e -> {
            app.getFoolproofModule().updateAll();
            if (e.getClickCount() == 2) {
                controller.processEditTA();
            }
            controller.processSelectTA();
        });

        RadioButton allRadio = (RadioButton) gui.getGUINode(OH_ALL_RADIO_BUTTON);
        allRadio.setOnAction(e -> {
            controller.processSelectAllTAs();
        });
        RadioButton gradRadio = (RadioButton) gui.getGUINode(OH_GRAD_RADIO_BUTTON);
        gradRadio.setOnAction(e -> {
            controller.processSelectGradTAs();
        });
        RadioButton undergradRadio = (RadioButton) gui.getGUINode(OH_UNDERGRAD_RADIO_BUTTON);
        undergradRadio.setOnAction(e -> {
            controller.processSelectUndergradTAs();
        });
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(OH_FOOLPROOF_SETTINGS,
                new OfficeHoursFoolproofDesign((CourseSiteGeneratorApp) app));
    }
    @Override
    public void showNewDialog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
     
    
}
