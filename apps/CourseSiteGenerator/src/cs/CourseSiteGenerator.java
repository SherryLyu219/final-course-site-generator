/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs;

import cs.data.CourseSiteData;
import cs.workspace.CourseSiteGeneratorWorkspace;
import djf.AppTemplate;
import djf.components.AppClipboardComponent;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import djf.components.AppWorkspaceComponent;
import java.util.Locale;
import static javafx.application.Application.launch;
/**
 *
 * @author lvxinrui
 */
public class CourseSiteGenerator extends AppTemplate{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Locale.setDefault(Locale.US);
       launch(args);
    }
   @Override
    public AppWorkspaceComponent buildWorkspaceComponent(AppTemplate app) {
       return new CourseSiteGeneratorWorkspace(this);        
    }
    @Override
    public AppClipboardComponent buildClipboardComponent(AppTemplate app) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public AppDataComponent buildDataComponent(AppTemplate app) {
        return new CourseSiteData();
    }

    @Override
    public AppFileComponent buildFileComponent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
