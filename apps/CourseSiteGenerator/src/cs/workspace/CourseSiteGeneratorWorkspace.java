/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cs.workspace;

import cs.CourseSiteGenerator;
import static cs.CourseSiteGeneratorProperty.CS_TAB_PANE;
import static cs.CourseSiteGeneratorProperty.OH_LEFT_PANE;
import static cs.CourseSiteGeneratorProperty.OH_TA_EDIT_DIALOG;
import static cs.workspace.Style.csgStyle.CLASS_CS_PANE;
import static cs.workspace.Style.csgStyle.CLASS_OH_PANE;
import cs.wrokspace.dialogs.CSDialog;
import djf.components.AppWorkspaceComponent;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;

/**
 *
 * @author lvxinrui
 */
public class CourseSiteGeneratorWorkspace extends AppWorkspaceComponent{
    public CourseSiteGeneratorWorkspace(CourseSiteGenerator app) {
        super(app);

        // LAYOUT THE APP
        initLayout();

        // INIT THE EVENT HANDLERS
        //initControllers();

        // 
        //initFoolproofDesign();

        // INIT DIALOGS
        //initDialogs();
    }
    /*
     private void initDialogs() {
        CSDialog taDialog = new CSDialog((CourseSiteGenerator) app);
        app.getGUIModule().addDialog(OH_TA_EDIT_DIALOG, taDialog);
    }
     */
     private void initLayout(){
          PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppNodesBuilder csBuilder = app.getGUIModule().getNodesBuilder();
        
        TabPane layout = csBuilder.bulidTabPane(CS_TAB_PANE, null,CLASS_CS_PANE,ENABLED);
//         VBox leftPane = csBuilder.buildVBox(OH_LEFT_PANE, null, CLASS_OH_PANE, ENABLED);
        Tab tab = new Tab("Site");
        layout.getTabs().add(tab);
        
         workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        ((BorderPane) workspace).setCenter(layout);
     }

    @Override
    public void showNewDialog() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
